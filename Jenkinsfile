pipeline {
   agent any

   tools {
      maven "Maven 3.8.4"
      jdk "JDK 17"
   }

   environment {
      GIT_APPLICATION_NAME="c4t"
      GIT_REPOSITORY_NAME="contracts-ui-manager"
      IMAGE_NAME="ecs-globpidck01.otc.ihkmun/${GIT_APPLICATION_NAME}/${GIT_REPOSITORY_NAME}"
      IMAGE_NAME_DOCKER_HUB="ihkmuenchen/${GIT_APPLICATION_NAME}-${GIT_REPOSITORY_NAME}"
      VERSION_NUMBER="master-staging"
   }

   stages {

      stage('Set version for tag') {
         when {
            tag "v*"
         }
         environment {
            VERSION_NUMBER="${sh(script:'echo $TAG_NAME | sed "s/v//"', returnStdout: true).trim()}"
         }
         steps {
             sh 'mvn versions:set -DnewVersion=$VERSION_NUMBER'
         }
      }

      stage('Set version for main') {
          when {
              branch "main"
          }
          steps {
              sh 'mvn versions:set -DnewVersion=staging'
          }
      }

      stage('Maven build') {
         steps {
             echo "GIT_URL               : $GIT_URL"
             echo "GIT_APPLICATION_NAME  : $GIT_APPLICATION_NAME"
             echo "GIT_REPOSITORY_NAME   : $GIT_REPOSITORY_NAME"
             echo "IMAGE_NAME            : $IMAGE_NAME"
             echo "IMAGE_NAME_DOCKER_HUB : $IMAGE_NAME_DOCKER_HUB"
             sh 'mvn clean verify -Dhttps.proxyHost=10.155.80.28 -Dhttps.proxyPort=3128 '
          }
      }

      stage('Sonar Analysis') {
        steps {
           withSonarQubeEnv('sonarqube-dev') {
              sh 'mvn sonar:sonar'
          }
        }
      }

      stage('OWASP Dependency Check') {
         steps {
            sh 'mvn org.owasp:dependency-check-maven:check'
         }
      }

      stage('Quality Gate') {
         steps {
            timeout(time: 10, unit: 'MINUTES') {
               sleep(10)
               waitForQualityGate abortPipeline: true
            }
         }
      }

      stage('Build image from branch') {
         when {
            anyOf {
               branch "main"
               triggeredBy cause: "UserIdCause"
            }
            not {
               tag "v*"
            }
         }
         steps {
            script {
               docker.withRegistry('https://ecs-globpidck01.otc.ihkmun', 'global_docker') {
                    sh "docker build --no-cache --pull -t $IMAGE_NAME:$VERSION_NUMBER ."
                    sh "docker push $IMAGE_NAME:$VERSION_NUMBER"
                    sh "docker tag $IMAGE_NAME:$VERSION_NUMBER $IMAGE_NAME_DOCKER_HUB:latest"
               }
            }
            script {
               docker.withRegistry('https://index.docker.io/v1/', 'global_docker_hub_ihkmuenchen') {
                   sh  "docker push     $IMAGE_NAME_DOCKER_HUB:latest"
               }
            }
            script {
                sh "export https_proxy=http://10.155.80.28:3128 && \
                    export HTTPS_PROXY=http://10.155.80.28:3128 && \
                    syft $IMAGE_NAME:$VERSION_NUMBER -o cyclonedx-json > bom.json"
            }
            withCredentials([string(credentialsId: 'dt-key', variable: 'API_KEY')]) {
                dependencyTrackPublisher artifact: 'bom.json', projectName: '$GIT_APPLICATION_NAME/$GIT_REPOSITORY_NAME', projectVersion: '$VERSION_NUMBER', synchronous: true, dependencyTrackApiKey: API_KEY, projectProperties: [tags:['spring-boot','eclipse-temurin:17-jre-alpine']]
            }
         }
      }

      stage('Build image from tag') {
         when {
            tag "v*"
         }
         environment {
            VERSION_NUMBER="${sh(script:'echo $TAG_NAME | sed "s/v//"', returnStdout: true).trim()}"
         }
         steps {
            script {
               docker.withRegistry('https://ecs-globpidck01.otc.ihkmun', 'global_docker') {
                  sh "docker build --no-cache --pull -t $IMAGE_NAME:$VERSION_NUMBER ."
                  sh "docker push     $IMAGE_NAME:$VERSION_NUMBER"
                  sh "docker tag      $IMAGE_NAME:$VERSION_NUMBER $IMAGE_NAME_DOCKER_HUB:$VERSION_NUMBER"
               }
            }
            script {
               docker.withRegistry('https://index.docker.io/v1/', 'global_docker_hub_ihkmuenchen') {
                   sh  "docker push   $IMAGE_NAME_DOCKER_HUB:$VERSION_NUMBER"
               }
            }
            script {
                sh "export https_proxy=http://10.155.80.28:3128 && \
                    export HTTPS_PROXY=http://10.155.80.28:3128 && \
                    syft $IMAGE_NAME:$VERSION_NUMBER -o cyclonedx-json > bom.json"
            }
            withCredentials([string(credentialsId: 'dt-key', variable: 'API_KEY')]) {
                dependencyTrackPublisher artifact: 'bom.json', projectName: '$GIT_APPLICATION_NAME/$GIT_REPOSITORY_NAME', projectVersion: '$VERSION_NUMBER', synchronous: true, dependencyTrackApiKey: API_KEY, projectProperties: [tags:['spring-boot','eclipse-temurin:17-jre-alpine']]
            }
         }
      }
   }
}
