FROM eclipse-temurin:21-jre-alpine

RUN apk --update add fontconfig ttf-dejavu curl

RUN adduser -D -g "" -h /home/ihk -G users ihk

RUN chmod -R 777 /var/cache/fontconfig && chown ihk:users /var/cache/fontconfig

WORKDIR /home/ihk

USER ihk

RUN mkdir -p /home/ihk/lib

ADD target/beteigeuze.jar /home/ihk/lib/beteigeuze.jar

CMD ["java", "-jar","/home/ihk/lib/beteigeuze.jar", "--spring.config.location=optional:classpath:/,optional:classpath:/config/,optional:file:/home/ihk/etc/"]
