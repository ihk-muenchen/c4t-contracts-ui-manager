/*
 * MIT License
 *
 * Copyright (c) 2023 Industrie- und Handelskammer für München und Oberbayern
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ihk.muenchen;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

@WebFluxTest(ApplicationUIService.class)
class StaticWebConfigurationTest {

    @Autowired
    private WebTestClient testClient;

    @Test
    @WithMockUser(username = "admin", password = "admin123")
    void loadIndexHtml_contracts() {
        WebTestClient.ResponseSpec responseSpec = this.testClient.mutateWith(csrf()).get().uri("/contracts/0x1")
                .exchange();

        responseSpec.expectStatus().isOk();
    }

    @Test
    @WithMockUser(username = "admin", password = "admin123")
    void loadIndexHtml_error() {
        WebTestClient.ResponseSpec responseSpec = this.testClient.mutateWith(csrf()).get().uri("/error")
                .exchange();

        responseSpec.expectStatus().isOk();
    }

    @Test
    @WithMockUser(username = "admin", password = "admin123")
    void doApiCall_unsupportedChar() {
        WebTestClient.ResponseSpec responseSpec = this.testClient.get().uri("/home/\\.\\")
                .exchange();

        responseSpec.expectStatus().isBadRequest();
    }
}