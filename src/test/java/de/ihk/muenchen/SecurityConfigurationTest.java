package de.ihk.muenchen;/*
 * MIT License
 *
 * Copyright (c) 2023 Industrie- u. Handelskammer f. Muenchen u. Oberbayern K.d.oe.R.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.HttpURLConnection;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = ApplicationUIService.class)
@Execution(ExecutionMode.SAME_THREAD)
@SuppressWarnings("java:S5778")
class SecurityConfigurationTest {

    @LocalServerPort
    private int port;

    private RestTemplate restTemplate;

    @BeforeEach
    void setup() {
        restTemplate = new RestTemplate(new SimpleClientHttpRequestFactory() {
            @Override
            protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                super.prepareConnection(connection, httpMethod);
                connection.setInstanceFollowRedirects(false);
            }
        });
    }

    @Test
    void load_actuator_without_valid_user_401() {
        // when
        HttpClientErrorException result = assertThrows(HttpClientErrorException.class, () ->
                restTemplate.exchange("http://localhost:" + port + "/actuator",
                        HttpMethod.GET,
                        new HttpEntity<>(null),
                        String.class)
        );
        // then
        assertEquals(HttpStatus.UNAUTHORIZED, result.getStatusCode());
    }


    @Test
    void load_actuator_with_invalid_user_401() {
        // when
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, buildAuthorizationHeader("admin", "xxxx"));
        HttpClientErrorException result = assertThrows(HttpClientErrorException.class, () ->
                restTemplate.exchange("http://localhost:" + port + "/actuator",
                        HttpMethod.GET,
                        new HttpEntity<>(headers),
                        String.class)
        );
        // then
        assertEquals(HttpStatus.UNAUTHORIZED, result.getStatusCode());
    }

    @Test
    void load_actuator_with_valid_user_200() {
        // when
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, buildAuthorizationHeader("admin", "admin123"));
        ResponseEntity<String> result = restTemplate.exchange("http://localhost:" + port + "/actuator",
                HttpMethod.GET,
                new HttpEntity<>(headers),
                String.class);

        // then
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    void load_health_needs_no_user_200() {
        // when
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<String> result = restTemplate.exchange("http://localhost:" + port + "/actuator/health",
                HttpMethod.GET,
                new HttpEntity<>(headers),
                String.class);

        // then
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertTrue(result.getBody().startsWith("{\"status\":"));
    }

    @Test
    void checkActuatorHealthLiveness_200_withoutUser() {
        // when
        ResponseEntity<String> result = restTemplate.exchange("http://localhost:" + port + "/actuator/health/liveness",
                HttpMethod.GET,null,
                String.class);

        // then
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("{\"status\":\"", result.getBody().toString().substring(0, 11));
    }

    @Test
    void checkActuatorHealthReadiness_200_withoutUser() {
        // when
        ResponseEntity<String> result = restTemplate.exchange("http://localhost:" + port + "/actuator/health/readiness",
                HttpMethod.GET,null,
                String.class);

        // then
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("{\"status\":\"", result.getBody().toString().substring(0, 11));
    }

    private String buildAuthorizationHeader(String username, String password) {
        return "Basic " + new String(Base64.encodeBase64((username + ":" + password).getBytes(), false));
    }

}
