// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
    config.set({
        plugins: [
            require('karma-jasmine'),
            require('karma-mocha-reporter'),
            require('karma-jasmine-diff-reporter'),
            require('karma-chrome-launcher'),
            require('karma-jasmine-html-reporter'),
            require('karma-sonarqube-reporter'),
            require('karma-coverage'),
            require('@angular-devkit/build-angular/plugins/karma'),
            require('karma-spec-reporter')
        ],

        autoWatch: true,
        singleRun: false,

        frameworks: ['jasmine', '@angular-devkit/build-angular'],
        basePath: '..',

        port: 9876,
        browsers: ['ChromeHeadlessNoSandbox'],
        customLaunchers: {
            ChromeHeadlessNoSandbox: {
                base: 'ChromeHeadless',
                flags: ['--no-sandbox']
            }
        },

        colors: true,
        logLevel: config.LOG_INFO,
        reporters: ['sonarqube', 'kjhtml', 'spec', 'mocha'],

        client: {
            clearContext: false, // leave Jasmine Spec Runner output visible in browser
            jasmine: {
                random: false
            }
        },

        jasmineDiffReporter: {
            color: {
                expectedBg: 'bgMagenta',
                expectedWhitespaceBg: 'bgMagenta',
                actualBg: 'bgBlue',
                actualWhitespaceBg: 'bgBlue'
            },
            legacy: true
        },

        mochaReporter: {
            output: 'minimal'
        },

        coverageReporter: {
            dir: require('path').join(__dirname, '../../../../target/coverage'),
            subdir: '.',
            reporters: [
                {type: 'html'},
                {type: 'lcov'}
            ],
            fixWebpackSourcePaths: true
        },

        sonarqubeReporter: {
            basePath: '',
            filePattern: '**/*spec.ts',
            encoding: 'utf-8',
            outputFolder: '../../../target/angular-junit-reports',
            legacyMode: true,
            reportName: 'TEST-Karma.xml'
        }

    });
};
