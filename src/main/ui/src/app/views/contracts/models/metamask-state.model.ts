export interface MetamaskState {
    isConnected?: boolean;
    isAccountValid?: boolean;
    isTransactionPending?: boolean;
}
