import {AbiItem} from 'web3';

export enum HashState {
    VALID = 'VALID',
    REVOKED = 'REVOKED',
    UNKNOWN = 'UNKNOWN'
}

export const NULL_ADDRESS = '0x0000000000000000000000000000000000000000';

export const HASH_VALUE_MANAGER_ABI: AbiItem[] = [
    {
        inputs: [],
        stateMutability: 'nonpayable',
        type: 'constructor'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: false,
                internalType: 'address',
                name: '_creator',
                type: 'address'
            },
            {
                indexed: false,
                internalType: 'string',
                name: 'version',
                type: 'string'
            }
        ],
        name: 'ConstructorDone',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: false,
                internalType: 'address',
                name: 'issuer',
                type: 'address'
            },
            {
                indexed: false,
                internalType: 'bytes32',
                name: '_hash',
                type: 'bytes32'
            }
        ],
        name: 'HashCreated',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: false,
                internalType: 'address',
                name: 'issuer',
                type: 'address'
            },
            {
                indexed: false,
                internalType: 'bytes32',
                name: '_hash',
                type: 'bytes32'
            }
        ],
        name: 'HashRemoved',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: false,
                internalType: 'address',
                name: 'issuer',
                type: 'address'
            },
            {
                indexed: false,
                internalType: 'bytes32',
                name: '_hash',
                type: 'bytes32'
            }
        ],
        name: 'HashRevalidated',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: false,
                internalType: 'address',
                name: 'issuer',
                type: 'address'
            },
            {
                indexed: false,
                internalType: 'bytes32',
                name: '_hash',
                type: 'bytes32'
            }
        ],
        name: 'HashRevoked',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: false,
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'Paused',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: true,
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'PauserAdded',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: true,
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'PauserRemoved',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: false,
                internalType: 'string',
                name: 'url',
                type: 'string'
            },
            {
                indexed: false,
                internalType: 'string',
                name: 'signature',
                type: 'string'
            }
        ],
        name: 'SignatureChanged',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: false,
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'Unpaused',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: true,
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'WhitelistAdminAdded',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: true,
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'WhitelistAdminRemoved',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: true,
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'WhitelistedAdded',
        type: 'event'
    },
    {
        anonymous: false,
        inputs: [
            {
                indexed: true,
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'WhitelistedRemoved',
        type: 'event'
    },
    {
        inputs: [
            {
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'addPauser',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [
            {
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'addWhitelistAdmin',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [
            {
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'addWhitelisted',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [
            {
                internalType: 'bytes32',
                name: 'hash',
                type: 'bytes32'
            }
        ],
        name: 'createHash',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [
            {
                internalType: 'bytes32',
                name: 'hash',
                type: 'bytes32'
            }
        ],
        name: 'getHashStatus',
        'outputs': [
            {
                internalType: 'address',
                name: 'issuer',
                type: 'address'
            },
            {
                internalType: 'bool',
                name: 'valid',
                type: 'bool'
            }
        ],
        stateMutability: 'view',
        type: 'function'
    },
    {
        inputs: [],
        name: 'getInvalidCount',
        'outputs': [
            {
                internalType: 'uint64',
                name: '',
                type: 'uint64'
            }
        ],
        stateMutability: 'view',
        type: 'function'
    },
    {
        inputs: [],
        name: 'getSignature',
        'outputs': [
            {
                internalType: 'string',
                name: '',
                type: 'string'
            }
        ],
        stateMutability: 'view',
        type: 'function'
    },
    {
        inputs: [],
        name: 'getUrl',
        'outputs': [
            {
                internalType: 'string',
                name: '',
                type: 'string'
            }
        ],
        stateMutability: 'view',
        type: 'function'
    },
    {
        inputs: [],
        name: 'getValidCount',
        'outputs': [
            {
                internalType: 'uint64',
                name: '',
                type: 'uint64'
            }
        ],
        stateMutability: 'view',
        type: 'function'
    },
    {
        inputs: [
            {
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'isPauser',
        'outputs': [
            {
                internalType: 'bool',
                name: '',
                type: 'bool'
            }
        ],
        stateMutability: 'view',
        type: 'function'
    },
    {
        inputs: [
            {
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'isWhitelistAdmin',
        'outputs': [
            {
                internalType: 'bool',
                name: '',
                type: 'bool'
            }
        ],
        stateMutability: 'view',
        type: 'function'
    },
    {
        inputs: [
            {
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'isWhitelisted',
        'outputs': [
            {
                internalType: 'bool',
                name: '',
                type: 'bool'
            }
        ],
        stateMutability: 'view',
        type: 'function'
    },
    {
        inputs: [],
        name: 'pause',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [],
        name: 'paused',
        'outputs': [
            {
                internalType: 'bool',
                name: '',
                type: 'bool'
            }
        ],
        stateMutability: 'view',
        type: 'function'
    },
    {
        inputs: [
            {
                internalType: 'bytes32',
                name: 'hash',
                type: 'bytes32'
            }
        ],
        name: 'removeHash',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [
            {
                internalType: 'address',
                name: 'account',
                type: 'address'
            }
        ],
        name: 'removeWhitelisted',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [],
        name: 'renouncePauser',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [],
        name: 'renounceWhitelistAdmin',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [],
        name: 'renounceWhitelisted',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [
            {
                internalType: 'bytes32',
                name: 'hash',
                type: 'bytes32'
            }
        ],
        name: 'revalidateHash',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [
            {
                internalType: 'bytes32',
                name: 'hash',
                type: 'bytes32'
            }
        ],
        name: 'revokeHash',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [
            {
                internalType: 'string',
                name: 'url',
                type: 'string'
            },
            {
                internalType: 'string',
                name: 'signature',
                type: 'string'
            }
        ],
        name: 'setSignature',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    },
    {
        inputs: [],
        name: 'unpause',
        'outputs': [],
        stateMutability: 'nonpayable',
        type: 'function'
    }
];
