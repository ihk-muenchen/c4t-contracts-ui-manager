import {Inject, Injectable, signal, WritableSignal} from '@angular/core';
import {MetamaskState} from '../models/metamask-state.model';
import Web3, {AbiItem, Contract} from 'web3';
import {WEB3} from '../../../shared/utils/web3';
import {HASH_VALUE_MANAGER_ABI} from '../models/abi.types';

@Injectable()
export class DocumentUploadService {

    metamaskState: WritableSignal<MetamaskState> = signal<MetamaskState>({isConnected: false});

    transactionDone: WritableSignal<boolean> = signal<boolean>(false);

    calculatedHash: WritableSignal<string> = signal<string>('');

    constructor(@Inject(WEB3) private readonly web3: Web3) {
    }

    getWeb3Provider(): Web3 {
        return this.web3;
    }

    createHashValueManagerContract(address: string): Contract<AbiItem[]> {
        return new this.web3.eth.Contract<AbiItem[]>(HASH_VALUE_MANAGER_ABI, address);
    }
}
