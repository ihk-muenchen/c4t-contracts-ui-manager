import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ContractsComponent} from './contracts.component';
import {ActivatedRoute} from '@angular/router';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('ContractsComponent (valid contract address)', () => {
    let component: ContractsComponent;
    let fixture: ComponentFixture<ContractsComponent>;

    const paramMap: Map<string, string> = new Map<string, string>([
        ['address', '0xd83b798648F5C1f2Ede9bd83fED1bCeF4eaECaAe']
    ]);

    beforeEach(() => {
        TestBed.configureTestingModule({
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA
            ],
            declarations: [ContractsComponent],
            providers: [
                {provide: ActivatedRoute, useValue: {snapshot: {paramMap: paramMap}}}
            ]
        });
        fixture = TestBed.createComponent(ContractsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
