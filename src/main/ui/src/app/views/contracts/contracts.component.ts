import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Utilities} from '../../shared/utils/utilities';

@Component({
    selector: 'app-contracts',
    templateUrl: './contracts.component.html'
})
export class ContractsComponent implements OnInit {

    contractAddress: string;
    isContractAddressValid: boolean;

    constructor(private readonly route: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.contractAddress = this.route.snapshot.paramMap.get('address');
        this.isContractAddressValid = Utilities.isContractAddressValid(this.contractAddress);
    }
}
