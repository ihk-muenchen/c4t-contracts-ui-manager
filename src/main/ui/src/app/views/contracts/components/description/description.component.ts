import {Component, OnInit} from '@angular/core';
import {LinkCreatorService} from '../../../../shared/services/link-creator.service';

@Component({
    selector: 'app-description',
    templateUrl: './description.component.html'
})
export class DescriptionComponent implements OnInit {

    onboardingCert4TrustLink: string;

    constructor(private readonly linkCreatorService: LinkCreatorService) {
    }

    ngOnInit(): void {
        this.prepareLinks();
    }

    private prepareLinks(): void {
        this.onboardingCert4TrustLink = this.linkCreatorService.prepareOnboardingCert4TrustLink();
    }
}
