import {ComponentFixture, TestBed} from '@angular/core/testing';
import {DescriptionComponent} from './description.component';
import {getTranslocoTestingModule} from '../../../../shared/modules/transloco-root.module';

describe('DescriptionComponent', () => {
    let component: DescriptionComponent;
    let fixture: ComponentFixture<DescriptionComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [getTranslocoTestingModule()],
            declarations: [DescriptionComponent]
        });
        fixture = TestBed.createComponent(DescriptionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
