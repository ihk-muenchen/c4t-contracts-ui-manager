import {ComponentFixture, TestBed} from '@angular/core/testing';
import {AccountDataComponent} from './account-data.component';
import {
    getTranslocoLocaleTestingProvider,
    getTranslocoTestingModule
} from '../../../../shared/modules/transloco-root.module';
import {DocumentUploadService} from '../../services/document-upload.service';
import {ActivatedRoute} from '@angular/router';
import {Web3} from 'web3';
import HDWalletProvider from '@truffle/hdwallet-provider';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

const prepareWeb3Provider = (): HDWalletProvider => {
    const myProvider = new HDWalletProvider({
        privateKeys: ['f0c3bcc5f51a1ac8e3114cd42aa84d3d559c1dd0cb15e60b3d1b95d1f1e24c31'],
        providerOrUrl: 'http://localhost:8545'
    });
    // @ts-expect-error Unrecognized Metamask property
    window.ethereum_testing_web3 = new Web3(myProvider);
    // @ts-expect-error Unrecognized Metamask property
    global.window.ethereum = {
        on: (eventName: string, callback: unknown) => {
            if (eventName === 'chainChanged') {
                // @ts-expect-error Callback function
                callback('0x539');
            } else {
                // @ts-expect-error Callback function
                callback(['0x94334a3261961b3EdD13D72cfd7E9651789E1217']);
            }
        },
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        request: (_functions: unknown) => new Promise((resolve, _reject) => {
            resolve(['0x94334a3261961b3EdD13D72cfd7E9651789E1217']);
        })
    };

    return myProvider;
};

const prepareWeb3ProviderWithUnsupportedNetwork = (): HDWalletProvider => {
    const myProvider = new HDWalletProvider({
        privateKeys: ['f0c3bcc5f51a1ac8e3114cd42aa84d3d559c1dd0cb15e60b3d1b95d1f1e24c31'],
        providerOrUrl: 'https://rpc.linea.build/'
    });
    // @ts-expect-error Unrecognized Metamask property
    window.ethereum_testing_web3 = new Web3(myProvider);
    // @ts-expect-error Unrecognized Metamask property
    global.window.ethereum = {
        on: (eventName: string, callback: unknown) => {
            if (eventName === 'chainChanged') {
                // @ts-expect-error Callback function
                callback('0x1');
            } else {
                // @ts-expect-error Callback function
                callback(['0x94334a3261961b3EdD13D72cfd7E9651789E1217']);
            }
        },
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        request: (_functions: unknown) => new Promise((resolve, _reject) => {
            resolve(['0x94334a3261961b3EdD13D72cfd7E9651789E1217']);
        })
    };

    return myProvider;
};

const injectWeb3Provider = (component: AccountDataComponent, provider): void => {
    component['web3'] = new Web3(provider);
};

describe('AccountDataComponent (metamask not connected)', () => {
    let component: AccountDataComponent;
    let fixture: ComponentFixture<AccountDataComponent>;

    const paramMap = new Map<string, string>([
        ['address', '0xd83b798648F5C1f2Ede9bd83fED1bCeF4eaECaAe']
    ]);

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [getTranslocoTestingModule()],
            declarations: [AccountDataComponent],
            providers: [
                getTranslocoLocaleTestingProvider(),
                DocumentUploadService,
                {provide: ActivatedRoute, useValue: {snapshot: {paramMap: paramMap}}}
            ]
        });
        fixture = TestBed.createComponent(AccountDataComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

describe('AccountDataComponent (metamask connected)', () => {
    let component: AccountDataComponent;
    let fixture: ComponentFixture<AccountDataComponent>;

    const paramMap = new Map<string, string>([
        ['address', '0xd83b798648F5C1f2Ede9bd83fED1bCeF4eaECaAe']
    ]);

    beforeEach(() => {
        const provider: HDWalletProvider = prepareWeb3Provider();

        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [getTranslocoTestingModule()],
            declarations: [AccountDataComponent],
            providers: [
                getTranslocoLocaleTestingProvider(),
                DocumentUploadService,
                {provide: ActivatedRoute, useValue: {snapshot: {paramMap: paramMap}}}
            ]
        });
        fixture = TestBed.createComponent(AccountDataComponent);
        component = fixture.componentInstance;
        injectWeb3Provider(component, provider);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should check balance when transaction is done', () => {
        const balanceSpy = spyOn(component['web3'].eth, 'getBalance').and.returnValue(Promise.resolve(1000));

        component.account = '0x94334a3261961b3EdD13D72cfd7E9651789E1217';
        component['documentUploadService'].transactionDone.set(true);
        fixture.detectChanges();

        expect(balanceSpy).toHaveBeenCalled();
    });
});

describe('AccountDataComponent (metamask connected - invalid contract address)', () => {
    let component: AccountDataComponent;
    let fixture: ComponentFixture<AccountDataComponent>;

    const paramMap = new Map<string, string>([
        ['address', '0xd83b798648F5C1f2Ede9bd83fED1bCeF4eaECaAf']
    ]);

    beforeEach(() => {
        const provider: HDWalletProvider = prepareWeb3Provider();

        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [getTranslocoTestingModule()],
            declarations: [AccountDataComponent],
            providers: [
                getTranslocoLocaleTestingProvider(),
                DocumentUploadService,
                {provide: ActivatedRoute, useValue: {snapshot: {paramMap: paramMap}}}
            ]
        });
        fixture = TestBed.createComponent(AccountDataComponent);
        component = fixture.componentInstance;
        injectWeb3Provider(component, provider);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

describe('AccountDataComponent (metamask connected - unsupportedNetwork)', () => {
    let component: AccountDataComponent;
    let fixture: ComponentFixture<AccountDataComponent>;

    const paramMap = new Map<string, string>([
        ['address', '0xd83b798648F5C1f2Ede9bd83fED1bCeF4eaECaAe']
    ]);

    beforeEach(() => {
        const provider: HDWalletProvider = prepareWeb3ProviderWithUnsupportedNetwork();

        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [getTranslocoTestingModule()],
            declarations: [AccountDataComponent],
            providers: [
                getTranslocoLocaleTestingProvider(),
                DocumentUploadService,
                {provide: ActivatedRoute, useValue: {snapshot: {paramMap: paramMap}}}
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(AccountDataComponent);
        component = fixture.componentInstance;
        injectWeb3Provider(component, provider);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
