import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HashActionComponent} from './hash-action.component';
import {DocumentUploadService} from '../../services/document-upload.service';
import {
    getTranslocoLocaleTestingProvider,
    getTranslocoTestingModule
} from '../../../../shared/modules/transloco-root.module';
import {ActivatedRoute} from '@angular/router';
import {NotificationService} from '../../../../shared/services/notification.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import HDWalletProvider from '@truffle/hdwallet-provider';
import {Web3} from 'web3';
import {HashState} from '../../models/abi.types';

const prepareWeb3Provider = (): HDWalletProvider => {
    const myProvider = new HDWalletProvider('91fd4e8a060cceff00ae5cde99d5b167179f724d9a424e24672e4200c7679c98', 'http://localhost:8545');
    // @ts-expect-error Untyped Metamask object
    window.ethereum_testing_web3 = new Web3(myProvider);
    // @ts-expect-error Untyped Metamask object
    global.window.ethereum = {
        on: (eventName: string, callback: unknown) => {
            if (eventName === 'chainChanged') {
                // @ts-expect-error Callback
                callback('0x539');
            } else {
                // @ts-expect-error Callback
                callback(['0xbD004d9048C9b9e5C4B5109c68dd569A65c47CF9']);
            }
        },
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        request: (_functions: unknown) => new Promise((resolve, _reject) => {
            resolve(['0xbD004d9048C9b9e5C4B5109c68dd569A65c47CF9']);
        })
    };

    return myProvider;
};

const injectWeb3Provider = (component: HashActionComponent, provider): void => {
    component['web3'] = new Web3(provider);
};

describe('HashActionComponent', () => {
    let component: HashActionComponent;
    let fixture: ComponentFixture<HashActionComponent>;

    const paramMap: Map<string, string> = new Map<string, string>([
        ['address', '0xd83b798648F5C1f2Ede9bd83fED1bCeF4eaECaAe']
    ]);

    beforeEach(() => {
        const provider: HDWalletProvider = prepareWeb3Provider();

        TestBed.configureTestingModule({
            imports: [
                getTranslocoTestingModule(),
                MatSnackBarModule,
                NoopAnimationsModule
            ],
            declarations: [HashActionComponent],
            providers: [
                getTranslocoLocaleTestingProvider(),
                DocumentUploadService,
                NotificationService,
                {provide: ActivatedRoute, useValue: {snapshot: {paramMap: paramMap}}}
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(HashActionComponent);
        component = fixture.componentInstance;
        injectWeb3Provider(component, provider);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        expect(component.hashState).toBeUndefined();
    });
});

describe('HashActionComponent (with calculated hash)', () => {
    let component: HashActionComponent;
    let fixture: ComponentFixture<HashActionComponent>;

    const paramMap = new Map<string, string>([
        ['address', '0xd83b798648F5C1f2Ede9bd83fED1bCeF4eaECaAe']
    ]);

    beforeEach(() => {
        const provider: HDWalletProvider = prepareWeb3Provider();

        TestBed.configureTestingModule({
            imports: [
                getTranslocoTestingModule(),
                MatSnackBarModule,
                NoopAnimationsModule
            ],
            declarations: [HashActionComponent],
            providers: [
                getTranslocoLocaleTestingProvider(),
                DocumentUploadService,
                NotificationService,
                {provide: ActivatedRoute, useValue: {snapshot: {paramMap: paramMap}}}
            ]
        }).compileComponents();
        const documentUploadService: DocumentUploadService = TestBed.inject(DocumentUploadService);
        documentUploadService.metamaskState.set({
            isConnected: true,
            isAccountValid: true
        });
        documentUploadService.calculatedHash.set('0x18871bc604b49d84eb0f84b0a9ce7883b726eb0f98c65f1797ea1774048b9177');

        fixture = TestBed.createComponent(HashActionComponent);
        component = fixture.componentInstance;
        injectWeb3Provider(component, provider);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        expect(component.hashState).toBeUndefined();
    });

    it('#createHash() should create hash', async () => {
        await component.createHash();
        await component.createHash();
        expect(component.hashState).toEqual(HashState.VALID);
    });

    it('#revokeHash() should revoke hash', async () => {
        await component.revokeHash();
        await component.revokeHash();
        expect(component.hashState).toEqual(HashState.REVOKED);
    });

    it('#revalidateHash() should revalidate hash', async () => {
        await component.revalidateHash();
        await component.revalidateHash();
        expect(component.hashState).toEqual(HashState.VALID);
    });

    it('#removeHash() should remove hash', async () => {
        await component.removeHash();
        await component.removeHash();
        expect(component.hashState).toEqual(HashState.UNKNOWN);
    });
});
