import {ChangeDetectorRef, Component, effect, inject, OnDestroy, OnInit} from '@angular/core';
import {AbiItem, Contract, Web3} from 'web3';
import {ChainId, SUPPORTED_CHAIN_ID, UNSUPPORTED_CHAIN_ID} from '../../../../shared/utils/web3';
import {DocumentUploadService} from '../../services/document-upload.service';
import {MetamaskState} from '../../models/metamask-state.model';
import {ActivatedRoute} from '@angular/router';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {TranslateService} from '../../../../shared/services/translate.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-account-data',
    templateUrl: './account-data.component.html',
    styleUrls: ['./account-data.component.scss']
})
export class AccountDataComponent implements OnInit, OnDestroy {

    metamask: unknown;
    isMetamaskConnected: boolean;

    account: string;
    balance: string;
    network: string;
    isValid: boolean;

    private web3: Web3;
    private hashValueManagerContract: Contract<AbiItem[]>;

    private langChangedSubscription: Subscription;

    constructor(private readonly documentUploadService: DocumentUploadService,
                private readonly cdr: ChangeDetectorRef,
                private readonly translateService: TranslateService) {
        const iconRegistry: MatIconRegistry = inject(MatIconRegistry);
        const sanitizer: DomSanitizer = inject(DomSanitizer);
        iconRegistry.addSvgIcon('valid', sanitizer.bypassSecurityTrustResourceUrl('assets/img/valid.svg'));
        iconRegistry.addSvgIcon('invalid', sanitizer.bypassSecurityTrustResourceUrl('assets/img/invalid.svg'));

        this.web3 = this.documentUploadService.getWeb3Provider();
        const route: ActivatedRoute = inject(ActivatedRoute);

        effect(() => {
            this.isMetamaskConnected = this.documentUploadService.metamaskState().isConnected;

            if (this.isMetamaskConnected) {
                const address: string = route.snapshot.paramMap.get('address');
                this.hashValueManagerContract = this.documentUploadService.createHashValueManagerContract(address);
            }

            const isTransactionDone: boolean = this.documentUploadService.transactionDone();

            if (isTransactionDone) {
                this.web3.eth.getBalance(this.account)
                    .then((balance: bigint) => this.balance = this.web3.utils.fromWei(balance, 'ether') + ' ETH');
            }
        });
    }

    ngOnInit(): void {
        this.metamask = window['ethereum'];

        if (this.metamask) {
            // @ts-expect-error Untyped Metamask object
            this.metamask.request({method: 'eth_requestAccounts'}).then();

            this.fetchMetaMaskData().then();

            // @ts-expect-error Untyped Metamask object
            this.metamask.on('accountsChanged', (accounts: string[]) => {
                this.documentUploadService.metamaskState.update((value: MetamaskState): MetamaskState => (
                    {...value, isConnected: accounts.length > 0}
                ));
                this.fetchMetaMaskData().then();
            });

            // @ts-expect-error Untyped Metamask object
            this.metamask.on('chainChanged', () => {
                this.isValid = false;
                this.fetchMetaMaskData().then();
            });
        }

        this.langChangedSubscription = this.translateService.langChanged()
            .subscribe(() => {
                this.network = this.network ?? this.translateService.translate('account-data.unknown-network');
            });
    }

    ngOnDestroy(): void {
        this.langChangedSubscription.unsubscribe();
    }

    private async fetchMetaMaskData(): Promise<void> {
        const accounts: string[] = await this.web3.eth.getAccounts();
        this.account = accounts[0];

        if (this.account) {
            this.documentUploadService.metamaskState.update((value: MetamaskState): MetamaskState => (
                {...value, isConnected: true}
            ));

            const balance: bigint = await this.web3.eth.getBalance(this.account);
            this.balance = this.web3.utils.fromWei(balance, 'ether') + ' ETH';

            const chainId: ChainId = (await this.web3.eth.getChainId() + '') as ChainId;
            this.network = SUPPORTED_CHAIN_ID.get(chainId);

            if (this.network) {
                this.checkAccountRole();
            } else {
                const unsupportedChainId: string = UNSUPPORTED_CHAIN_ID.get(chainId);
                if (unsupportedChainId){
                    const message: string = this.translateService.translate('account-data.unsupported-network');
                    this.network = `${unsupportedChainId} ${message}`;
                } else {
                    this.network = this.translateService.translate('account-data.unknown-network');
                }
                this.isValid = false;
                this.documentUploadService.metamaskState.update((value: MetamaskState): MetamaskState => ({
                    ...value, isAccountValid: false
                }));
            }
        }

        this.cdr.detectChanges();
    }

    private checkAccountRole(): void {
        this.hashValueManagerContract.methods.isWhitelisted(this.account)
            .call()
            .then((output) => {
                this.isValid = output as unknown as boolean;
                this.documentUploadService.metamaskState.update((value: MetamaskState) : MetamaskState => ({
                    ...value, isAccountValid: this.isValid
                }));
            })
            .catch(() => {
                this.documentUploadService.metamaskState.update((value: MetamaskState): MetamaskState => ({
                    ...value, isAccountValid: false
                }));
            });
    }
}
