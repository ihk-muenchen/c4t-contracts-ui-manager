import {ComponentFixture, TestBed} from '@angular/core/testing';
import {DocumentUploadComponent} from './document-upload.component';
import {getTranslocoTestingModule} from '../../../../shared/modules/transloco-root.module';
import {DocumentUploadService} from '../../services/document-upload.service';
import {ButtonComponent} from '../../../../shared/modules/app-common/components/button/button.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppCommonModule} from '../../../../shared/modules/app-common/app-common.module';

describe('DocumentUploadComponent', () => {
    let component: DocumentUploadComponent;
    let fixture: ComponentFixture<DocumentUploadComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                getTranslocoTestingModule(),
                ReactiveFormsModule,
                FormsModule,
                AppCommonModule
            ],
            declarations: [DocumentUploadComponent, ButtonComponent],
            providers: [DocumentUploadService]
        }).compileComponents();
        fixture = TestBed.createComponent(DocumentUploadComponent);
        component = fixture.componentInstance;
        component.metamaskState = {
            isConnected: true,
            isAccountValid: true
        };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should run #hashFile() with file in array', () => {
        const event = {
            target: {
                files: [
                    new File([''], 'test.txt')
                ]
            }
        } as unknown as Event;
        component.hashFile(event);
        expect(component.hashFC.value).toEqual('');
    });

    it('should run #hashFile() with empty files array', () => {
        const event = {
            target: {
                files: []
            }
        } as unknown as Event;
        component.hashFile(event);
        expect(component.hashFC.value).toEqual('');
    });
});
