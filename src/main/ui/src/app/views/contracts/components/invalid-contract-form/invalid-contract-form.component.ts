import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Utilities} from '../../../../shared/utils/utilities';
import {UnsupportedBrowserService} from '../../../../shared/services/unsupported-browser.service';
import {LinkCreatorService} from '../../../../shared/services/link-creator.service';

@Component({
    selector: 'app-invalid-contract-form',
    templateUrl: './invalid-contract-form.component.html',
    styleUrls: ['./invalid-contract-form.component.scss']
})
export class InvalidContractFormComponent implements OnInit {

    onboardingCert4TrustLink: string;

    formGroup: FormGroup;
    contractFC: FormControl<string>;

    isUnsupportedBrowser: boolean;

    constructor(@Inject('Window') private readonly window: Window,
                private readonly router: Router,
                private readonly formBuilder: FormBuilder,
                private readonly unsupportedBrowserService: UnsupportedBrowserService,
                private readonly linkCreatorService: LinkCreatorService) {
    }

    ngOnInit(): void {
        this.isUnsupportedBrowser = this.unsupportedBrowserService.isUnsupportedBrowser();

        this.contractFC = new FormControl<string>('', Validators.compose(
            [Validators.required, Utilities.addressValidator])
        );

        this.formGroup = this.formBuilder.group({
            contract: this.contractFC
        });

        this.prepareLinks();
    }

    checkContract(): void {
        const address: string = this.contractFC.value;
        this.router.navigate([`/contracts/${address}`]).then(() => this.window.location.reload());
    }

    private prepareLinks(): void {
        this.onboardingCert4TrustLink = this.linkCreatorService.prepareOnboardingCert4TrustLink();
    }
}
