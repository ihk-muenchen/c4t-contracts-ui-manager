import {ComponentFixture, TestBed} from '@angular/core/testing';
import {InvalidContractFormComponent} from './invalid-contract-form.component';
import {getTranslocoTestingModule} from '../../../../shared/modules/transloco-root.module';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('InvalidContractFormComponent', () => {
    let component: InvalidContractFormComponent;
    let fixture: ComponentFixture<InvalidContractFormComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA
            ],
            imports: [
                getTranslocoTestingModule(),
                ReactiveFormsModule,
                RouterTestingModule
            ],
            declarations: [InvalidContractFormComponent],
            providers: [
                {
                    provide: 'Window', useValue: {
                        location: {
                            // eslint-disable-next-line @typescript-eslint/no-empty-function
                            reload: () => {
                            }
                        }
                    }
                }
            ]
        });
        fixture = TestBed.createComponent(InvalidContractFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should navigate to correct contract address', () => {
        const routerSpy = spyOn(component['router'], 'navigate').and.returnValue(Promise.resolve(true));
        component.contractFC.patchValue('0x73b69304Ea866a8d4baE183d8728BFf35AD4d19B');

        component.checkContract();

        expect(routerSpy).toHaveBeenCalled();
    });
});
