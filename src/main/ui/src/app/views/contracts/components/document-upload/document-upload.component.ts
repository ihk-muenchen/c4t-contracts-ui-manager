import {Component, effect, inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {DocumentUploadService} from '../../services/document-upload.service';
import SHA3 from 'sha3';
import {MetamaskState} from '../../models/metamask-state.model';

@Component({
    selector: 'app-document-upload',
    templateUrl: './document-upload.component.html',
    styleUrls: ['./document-upload.component.scss']
})
export class DocumentUploadComponent implements OnInit {

    metamaskState: MetamaskState;

    hashForm: FormGroup;
    hashFC: FormControl<string>;

    fileName: string;

    constructor(private readonly formBuilder: FormBuilder,
                private readonly documentUploadService: DocumentUploadService) {
        const iconRegistry: MatIconRegistry = inject(MatIconRegistry);
        const sanitizer: DomSanitizer = inject(DomSanitizer);

        iconRegistry.addSvgIcon('pdf', sanitizer.bypassSecurityTrustResourceUrl('assets/img/pdf.svg'));

        effect(() => {
            this.metamaskState = this.documentUploadService.metamaskState();
        });
    }

    ngOnInit(): void {
        this.hashFC = new FormControl<string>('', Validators.required);

        this.hashForm = this.formBuilder.group({
            hash: this.hashFC
        });
    }

    hashFile(event: Event): void {
        const fileReader= new FileReader();
        // @ts-expect-error Generic HTML event
        const file = event.target.files[0] as File;
        if (file) {
            this.fileName = file.name;
            fileReader.onloadend = () => {
                const hasher = new SHA3(256);
                // @ts-expect-error Untyped FileReader
                hasher.update(fileReader.result, 'binary');
                const hash: string = '0x' + hasher.digest('hex');
                this.hashFC.patchValue(hash);
                this.documentUploadService.calculatedHash.set(hash);
            };
            fileReader.readAsBinaryString(file);
        } else {
            this.fileName = '';
            this.hashFC.patchValue('');
            this.documentUploadService.calculatedHash.set('');
        }
    }
}
