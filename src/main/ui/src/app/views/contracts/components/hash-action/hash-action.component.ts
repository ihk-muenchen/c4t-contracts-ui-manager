import {Component, effect, inject} from '@angular/core';
import {DocumentUploadService} from '../../services/document-upload.service';
import {AbiItem, Contract, ContractMethod, Web3} from 'web3';
import {ActivatedRoute} from '@angular/router';
import {MetamaskState} from '../../models/metamask-state.model';
import {HashState, NULL_ADDRESS} from '../../models/abi.types';
import {NonPayableMethodObject, PayableMethodObject} from 'web3-eth-contract';
import {NotificationService} from '../../../../shared/services/notification.service';
import {MatSnackBarRef, TextOnlySnackBar} from '@angular/material/snack-bar';
import {AbiFunctionFragment} from 'web3-types/src/eth_abi_types';

@Component({
    selector: 'app-hash-action',
    templateUrl: './hash-action.component.html',
    styleUrls: ['./hash-action.component.scss']
})
export class HashActionComponent {

    calculatedHash: string;
    hashState: HashState;
    isTransactionPending: boolean;

    HashState = HashState;

    private web3: Web3;
    private hashValueManagerContract: Contract<AbiItem[]>;

    private transactionNotificationRef: MatSnackBarRef<TextOnlySnackBar>;

    constructor(private readonly documentUploadService: DocumentUploadService,
                private readonly notificationService: NotificationService) {
        const route: ActivatedRoute = inject(ActivatedRoute);
        const address: string = route.snapshot.paramMap.get('address');
        this.web3 = documentUploadService.getWeb3Provider();
        this.hashValueManagerContract = documentUploadService.createHashValueManagerContract(address);

        effect(() => {
            this.calculatedHash = documentUploadService.calculatedHash();
            const metamaskState: MetamaskState = documentUploadService.metamaskState();

            if (metamaskState.isConnected && metamaskState.isAccountValid && this.calculatedHash) {
                this.checkState();
            } else {
                this.hashState = undefined;
            }
        });
    }

    private static certificateResultCheck(output: unknown): HashState {
        const hash: string = output[0];
        const state: boolean = output[1];

        if (state) {
            return HashState.VALID;
        }
        if (NULL_ADDRESS === hash) {
            return HashState.UNKNOWN;
        }
        return HashState.REVOKED;
    }

    public async createHash(): Promise<void> {
        const successMessage = 'contracts.hash-action.notification.hash-created';
        await this.sendTransaction(this.hashValueManagerContract.methods.createHash(this.calculatedHash), successMessage);
    }

    public async revokeHash(): Promise<void> {
        const successMessage = 'contracts.hash-action.notification.hash-revoked';
        await this.sendTransaction(this.hashValueManagerContract.methods.revokeHash(this.calculatedHash), successMessage);
    }

    public async revalidateHash(): Promise<void> {
        const successMessage = 'contracts.hash-action.notification.hash-revalidated';
        await this.sendTransaction(this.hashValueManagerContract.methods.revalidateHash(this.calculatedHash), successMessage);
    }

    public async removeHash(): Promise<void> {
        const successMessage = 'contracts.hash-action.notification.hash-removed';
        await this.sendTransaction(this.hashValueManagerContract.methods.removeHash(this.calculatedHash), successMessage);
    }

    private checkState(): void {
        this.hashValueManagerContract.methods.getHashStatus(this.calculatedHash)
            .call()
            .then((output) => this.hashState = HashActionComponent.certificateResultCheck(output))
            .catch(err => console.error(err));
    }

    private async sendTransaction(method: PayableMethodObject<ContractMethod<AbiFunctionFragment>['Inputs'],
        ContractMethod<AbiFunctionFragment>['Outputs']> |
        NonPayableMethodObject<ContractMethod<AbiFunctionFragment>['Inputs'],
            ContractMethod<AbiFunctionFragment>['Outputs']>, successMessage: string): Promise<void> {
        const accounts: string[] = await this.web3.eth.getAccounts();
        const account: string = accounts[0];

        const estimatedGasPrice: bigint = await this.web3.eth.estimateGas({from: account});
        const defaultGasPrice = BigInt(this.web3.utils.toWei(1, 'gwei'));

        const gasPrice: string = (estimatedGasPrice > defaultGasPrice ? estimatedGasPrice : defaultGasPrice) + '';

        await method.send({
            from: account,
            gasPrice: gasPrice
        })
            .on('transactionHash', () => {
                this.isTransactionPending = true;
                this.documentUploadService.metamaskState.update((value: MetamaskState): MetamaskState => ({
                    ...value, isTransactionPending: true
                }));
                this.transactionNotificationRef = this.notificationService
                    .success('contracts.hash-action.notification.perform-transaction-success', null, 999_000);
            })
            .then(() => {
                this.checkState();
                this.documentUploadService.transactionDone.set(true);
                this.transactionNotificationRef.dismiss();
                this.notificationService.success(successMessage);
            })
            .catch(err => {
                this.transactionNotificationRef?.dismiss();
                this.notificationService.error(err.message);
            })
            .finally(() => {
                this.documentUploadService.transactionDone.set(false);
                this.isTransactionPending = false;
                this.documentUploadService.metamaskState.update((value: MetamaskState): MetamaskState => ({
                    ...value, isTransactionPending: false
                }));
            });
    }
}
