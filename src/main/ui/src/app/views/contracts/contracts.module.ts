import {NgModule} from '@angular/core';
import {AppCommonModule} from '../../shared/modules/app-common/app-common.module';
import {ContractsRoutingModule} from './contracts-routing.module';
import {TRANSLOCO_SCOPE, TranslocoModule} from '@ngneat/transloco';
import {ContractsComponent} from './contracts.component';
import {DescriptionComponent} from './components/description/description.component';
import {AccountDataComponent} from './components/account-data/account-data.component';
import {DocumentUploadComponent} from './components/document-upload/document-upload.component';
import {ReactiveFormsModule} from '@angular/forms';
import {DocumentUploadService} from './services/document-upload.service';
import {HashActionComponent} from './components/hash-action/hash-action.component';
import {InvalidContractFormComponent} from './components/invalid-contract-form/invalid-contract-form.component';

@NgModule({
    imports: [
        AppCommonModule,
        ContractsRoutingModule,
        TranslocoModule,
        ReactiveFormsModule
    ],
    providers: [
        DocumentUploadService,
        {provide: TRANSLOCO_SCOPE, useValue: {scope: 'contracts', alias: 'contracts'}},
        {provide: 'Window', useValue: window}
    ],
    declarations: [
        ContractsComponent,
        DescriptionComponent,
        AccountDataComponent,
        DocumentUploadComponent,
        HashActionComponent,
        InvalidContractFormComponent
    ]
})
export class ContractsModule {
}
