import {Component, inject} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-not-found',
    templateUrl: './not-found.component.html',
    styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {

    constructor() {
        const iconRegistry: MatIconRegistry = inject(MatIconRegistry);
        const sanitizer: DomSanitizer = inject(DomSanitizer);
        
        iconRegistry.addSvgIcon('not-found', sanitizer.bypassSecurityTrustResourceUrl('assets/img/not-found.svg'));
    }
}
