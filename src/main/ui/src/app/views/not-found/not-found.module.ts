import {NgModule} from '@angular/core';
import {NotFoundRoutingModule} from './not-found-routing.module';
import {NotFoundComponent} from './not-found.component';
import {AppCommonModule} from '../../shared/modules/app-common/app-common.module';
import {TRANSLOCO_SCOPE, TranslocoModule} from '@ngneat/transloco';

@NgModule({
    imports: [
        AppCommonModule,
        NotFoundRoutingModule,
        TranslocoModule
    ],
    providers: [
        {provide: TRANSLOCO_SCOPE, useValue: {scope: 'not-found', alias: 'not-found'}}
    ],
    declarations: [
        NotFoundComponent,
    ]
})
export class NotFoundModule {
}
