import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {NotFoundComponent} from './not-found.component';
import {AppCommonModule} from '../../shared/modules/app-common/app-common.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {getTranslocoTestingModule} from '../../shared/modules/transloco-root.module';

describe('NotFoundComponent', () => {
    let component: NotFoundComponent;
    let fixture: ComponentFixture<NotFoundComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [
                HttpClientTestingModule,
                AppCommonModule,
                getTranslocoTestingModule()
            ],
            declarations: [NotFoundComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NotFoundComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
