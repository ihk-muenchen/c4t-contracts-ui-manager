import {TestBed, waitForAsync} from '@angular/core/testing';
import {NotFoundRoutingModule} from './not-found-routing.module';
import {RouterModule} from '@angular/router';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('NotFoundRoutingModule', () => {

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [NotFoundRoutingModule]
        });
    }));

    it('Should provide RouterModule', () => {
        expect(() => TestBed.inject(RouterModule)).toBeDefined();
    });
});
