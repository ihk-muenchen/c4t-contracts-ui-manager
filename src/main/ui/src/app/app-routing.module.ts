import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'contracts/',
        pathMatch: 'full'
    },
    {
        path: 'contracts',
        redirectTo: 'contracts/',
        pathMatch: 'full'
    },
    {
        path: 'contracts/:address',
        loadChildren: () => import('./views/contracts/contracts.module').then(m => m.ContractsModule)
    },
    {
        path: 'error',
        loadChildren: () => import('./views/not-found/not-found.module').then(m => m.NotFoundModule)
    },
    {
        path: '**',
        loadChildren: () => import('./views/not-found/not-found.module').then(m => m.NotFoundModule)
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
