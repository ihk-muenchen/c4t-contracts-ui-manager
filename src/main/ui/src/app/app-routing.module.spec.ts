import {Router} from '@angular/router';
import {TestBed, waitForAsync} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {routes} from './app-routing.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NotFoundModule} from './views/not-found/not-found.module';
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';

describe('Lazy Loading test cases', () => {

    let router: Router;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            // You need to config your TestBed and load any dependencies that are required by your Module.
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
            ]
        })
            .compileComponents();

        router = TestBed.inject(Router);
        router.resetConfig(routes);
        router.initialNavigation();
    }));

    describe('Route Error', () => {

        it('should load specific module', async () => {
            const route = router.config.find(rc => rc.path === 'error');
            if (typeof route.loadChildren === 'function') {
                expect(await route.loadChildren()).toEqual(NotFoundModule);
            }
        });

        it('should navigate to lazy loaded module', async () => {
            const result = await router.navigateByUrl('/error');
            expect(result).toBeTrue();
        });
    });

    describe('Route **', () => {

        it('should load specific module', async () => {
            const route = router.config.find(rc => rc.path === '**');
            if (typeof route.loadChildren === 'function') {
                expect(await route.loadChildren()).toEqual(NotFoundModule);
            }
        });

        it('should navigate to lazy loaded module', async () => {
            const result = await router.navigateByUrl('/test');
            expect(result).toBeTrue();
        });
    });
});
