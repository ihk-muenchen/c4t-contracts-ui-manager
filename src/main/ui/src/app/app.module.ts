import {BrowserModule} from '@angular/platform-browser';
import {enableProdMode, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {DatePipe} from '@angular/common';
import {AppCommonModule} from './shared/modules/app-common/app-common.module';
import {TranslocoRootModule} from './shared/modules/transloco-root.module';
import {LanguageSelectorComponent} from './components/language-selector/language-selector.component';

enableProdMode();

@NgModule({
    declarations: [
        AppComponent,
        LanguageSelectorComponent
    ],
    imports: [
        AppCommonModule,
        AppRoutingModule,
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        TranslocoRootModule,
    ],
    providers: [
        DatePipe,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
