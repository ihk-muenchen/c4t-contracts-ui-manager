import {Component, inject, Input, ViewChild} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {Utilities} from '../../../../utils/utilities';
import {NotificationService} from '../../../../services/notification.service';

@Component({
    selector: 'app-contract-address',
    templateUrl: './contract-address.component.html',
    styleUrls: ['./contract-address.component.scss']
})
export class ContractAddressComponent {

    @Input() hash = '-';

    @ViewChild('hashInput')
    hashInput: HTMLInputElement;

    constructor(private notificationService: NotificationService) {
        const iconRegistry: MatIconRegistry = inject(MatIconRegistry);
        const sanitizer: DomSanitizer = inject(DomSanitizer);

        iconRegistry.addSvgIcon('file_copy', sanitizer.bypassSecurityTrustResourceUrl('assets/img/file_copy-24px.svg'));
    }

    public isCopyIconVisible(): boolean {
        return (
            Utilities.isHashValid(this.hash) ||
            Utilities.isContractAddressValid(this.hash)
        );
    }

    public copyToClipboard() {
        const clipboardEvent = (event: ClipboardEvent) => {
            event.clipboardData.setData('text/plain', (this.hash));
            event.preventDefault();
            document.removeEventListener('copy', clipboardEvent);
        };
        document.addEventListener('copy', clipboardEvent);
        document.execCommand('copy'); // NOSONAR
        this.notificationService.default('notification.copy-to-clipboard');
    }

    public isHashSha3(): boolean {
        return Utilities.isHashValid(this.hash);
    }

    public getHashStart(): string {
        let text: string;
        switch (this.hash.length) {
            case 66:
                text = Utilities.formatHashStart(this.hash);
                break;
            case 42:
            default:
                text = Utilities.formatContractAddressStart(this.hash);
                break;
        }
        return text;
    }

    public getHashEnd(): string {
        let text: string;
        switch (this.hash.length) {
            case 66:
                text = Utilities.formatHashEnd(this.hash);
                break;
            case 42:
            default:
                text = Utilities.formatContractAddressEnd(this.hash);
                break;
        }
        return text;
    }
}
