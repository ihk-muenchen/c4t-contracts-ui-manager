import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ContractAddressComponent} from './contract-address.component';
import {MaterialModule} from '../../../material.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CUSTOM_ELEMENTS_SCHEMA, Injectable} from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {NotificationService} from '../../../../services/notification.service';


// @ts-expect-error Insufficient mock
class MockHTMLInputElement implements HTMLInputElement {

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    select(): void {
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars,@typescript-eslint/no-empty-function
    setSelectionRange(start: number, end: number, direction?: 'forward' | 'backward' | 'none'): void {
    }

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    blur(): void {
    }
}

class MockEvent {

    clipboardData = {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        setData: (format: string, data: string) => {
            // console.debug('setData');
        }
    };

    preventDefault() {
        // console.debug('preventDefault');
    }

}

@Injectable()
class MockNotificationService {

    static lastMessage = '';

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    default(message: string, action?: string, durationMs = 3000) {
        MockNotificationService.lastMessage = message;
    }
}

describe('ContractAddressComponent', () => {
    let component: ContractAddressComponent;
    let fixture: ComponentFixture<ContractAddressComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [
                NoopAnimationsModule,
                HttpClientTestingModule,
                MaterialModule
            ],
            providers: [
                {
                    provide: NotificationService,
                    useClass: MockNotificationService
                }
            ],
            declarations: [
                ContractAddressComponent
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ContractAddressComponent);
        component = fixture.componentInstance;

        fixture.detectChanges();
    });

    it('should run #constructor', () => {
        expect(component).toBeTruthy();
    });

    it('should run #getHashStart() 66 chars', () => {
        component.hash = '0x9f35cd1f6be400932cc7a0c55fb97d1e13ce1d35e137cda694ad6849ce506768';
        expect(component.getHashStart()).toEqual('0x9f35cd1f6be400932cc7a0c55fb97d1');
    });

    it('should run #getHashStart() 42 chars', () => {
        component.hash = '0x9f35cd1f6be400932cc7a0c55fb97d1e13ce1d35';
        expect(component.getHashStart()).toEqual('0x9f35cd1f6be400932cc');
    });

    it('should run #getHashEnd() 66 chars', () => {
        component.hash = '0x9f35cd1f6be400932cc7a0c55fb97d1e13ce1d35e137cda694ad6849ce506768';
        expect(component.getHashEnd()).toEqual('e13ce1d35e137cda694ad6849ce506768');
    });

    it('should run #getHashEnd() 42 chars', () => {
        component.hash = '0x9f35cd1f6be400932cc7a0c55fb97d1e13ce1d35';
        expect(component.getHashEnd()).toEqual('7a0c55fb97d1e13ce1d35');
    });

    it('should run #copyToClipboard() ', () => {
        component.hash = '0x9f35cd1f6be400932cc7a0c55fb97d1e13ce1d35';
        const htmlInputMock = new MockHTMLInputElement();
        spyOn(htmlInputMock, 'blur');
        spyOn(htmlInputMock, 'select');
        spyOn(htmlInputMock, 'setSelectionRange');

        spyOn(document, 'addEventListener').and.callFake((type, clipboardEventHandler) => {
            clipboardEventHandler(new MockEvent());
        });

        component.copyToClipboard();
        expect(MockNotificationService.lastMessage).toEqual('notification.copy-to-clipboard');
        expect(htmlInputMock.blur).toHaveBeenCalledTimes(0);
        expect(htmlInputMock.select).toHaveBeenCalledTimes(0);
        expect(htmlInputMock.setSelectionRange).toHaveBeenCalledTimes(0);
    });

    it('should run #copyToClipboard() not IE', () => {
        component.copyToClipboard();
        expect(component.hashInput).toBeDefined();
    });

});
