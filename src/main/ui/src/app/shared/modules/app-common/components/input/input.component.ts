import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.scss']
})
export class InputComponent {

    @Input() label = '';
    @Input() placeholder = '';
    @Input() value = '';
    @Input() readonly = false;
    @Input() paddingLeft = false;
}
