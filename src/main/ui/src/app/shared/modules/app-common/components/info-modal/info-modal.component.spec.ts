import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {InfoModalComponent} from './info-modal.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {MaterialModule} from '../../../material.module';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {getTranslocoTestingModule} from '../../../transloco-root.module';


const dialogMock = {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    close: () => {
    }
};

describe('InfoModalComponent', () => {
    let component: InfoModalComponent;
    let fixture: ComponentFixture<InfoModalComponent>;


    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [
                MatDialogModule,
                MaterialModule,
                getTranslocoTestingModule({translocoConfig: {defaultLang: 'de'}})
            ],
            declarations: [
                InfoModalComponent,
            ],
            providers: [
                {
                    provide: MatDialogRef,
                    useValue: dialogMock
                },
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: {}
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(InfoModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('dialog should be closed after onCloseClick()', () => {
        const spy = spyOn(component.dialogRef, 'close').and.callThrough();
        component.onCloseClick();
        expect(spy).toHaveBeenCalled();
    });

});
