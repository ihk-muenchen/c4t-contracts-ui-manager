import {NgModule} from '@angular/core';
import {ButtonComponent} from './components/button/button.component';
import {ContractAddressComponent} from './components/contract-address/contract-address.component';
import {InputComponent} from './components/input/input.component';
import {TitleComponent} from './components/title/title.component';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material.module';
import {InfoModalComponent} from './components/info-modal/info-modal.component';
import {TranslocoModule} from '@ngneat/transloco';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        TranslocoModule
    ],
    exports: [
        ButtonComponent,
        ContractAddressComponent,
        InputComponent,
        TitleComponent,
        MaterialModule,
        InfoModalComponent,
    ],
    declarations: [
        ButtonComponent,
        ContractAddressComponent,
        InputComponent,
        TitleComponent,
        InfoModalComponent,
    ]
})
export class AppCommonModule {
}
