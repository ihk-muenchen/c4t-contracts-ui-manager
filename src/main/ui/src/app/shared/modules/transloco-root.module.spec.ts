import {
    TranslocoCustomInterceptor,
    TranslocoCustomMissingHandler,
    TranslocoCustomStrategy,
    TranslocoHttpLoader
} from './transloco-root.module';
import {TestBed, waitForAsync} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Translation, TranslocoMissingHandlerData} from '@ngneat/transloco';

describe('TranslocoHttpLoader', () => {

    let loader: TranslocoHttpLoader;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ]
        });
    }));

    beforeEach(() => {
        loader = TestBed.inject(TranslocoHttpLoader);
    });

    it('should create an instance', () => {
        expect(loader).toBeTruthy();
    });

    it('should get translation file "de"', () => {
        let content: string;
        loader.getTranslation('de')
            .subscribe(file => {
                content = JSON.stringify(file);
                expect(content).toBeTruthy();
                expect(content.includes('Anmelden')).toBeTruthy();
            });
        expect(loader).toBeTruthy();
    });

    it('should get translation file "en"', () => {
        let content: string;
        loader.getTranslation('de')
            .subscribe(file => {
                content = JSON.stringify(file);
                expect(content).toBeTruthy();
                expect(content.includes('Register')).toBeTruthy();
            });
        expect(loader).toBeTruthy();
    });
});

describe('TranslocoCustomMissingHandler', () => {

    let missingHandler: TranslocoCustomMissingHandler;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ]
        });
    }));

    beforeEach(() => {
        missingHandler = TestBed.inject(TranslocoCustomMissingHandler);
    });

    it('should create an instance', () => {
        expect(missingHandler).toBeTruthy();
    });

    it('#handle() should not return translated English message (incorrect key)', () => {
        const data: TranslocoMissingHandlerData = {
            availableLangs: undefined,
            failedRetries: 0,
            flatten: {aot: false},
            interpolation: ['', ''],
            missingHandler: {allowEmpty: false, logMissingKey: false, useFallbackTranslation: false},
            prodMode: false,
            reRenderOnLangChange: false,
            activeLang: 'en',
            defaultLang: 'de'
        };
        const message: string = missingHandler.handle('notification.api', data);
        expect(message).toEqual('notification.api');
    });

    it('#handle() should not return translated German message (incorrect key)', () => {
        const data: TranslocoMissingHandlerData = {
            availableLangs: undefined,
            failedRetries: 0,
            flatten: {aot: false},
            interpolation: ['', ''],
            missingHandler: {allowEmpty: false, logMissingKey: false, useFallbackTranslation: false},
            prodMode: false,
            reRenderOnLangChange: false,
            activeLang: 'de',
            defaultLang: 'de'
        };
        const message: string = missingHandler.handle('notification.api', data);
        expect(message).toEqual('notification.api');
    });

    it('#handle() should return translated English message', () => {
        const data: TranslocoMissingHandlerData = {
            availableLangs: undefined,
            failedRetries: 0,
            flatten: {aot: false},
            interpolation: ['', ''],
            missingHandler: {allowEmpty: false, logMissingKey: false, useFallbackTranslation: false},
            prodMode: false,
            reRenderOnLangChange: false,
            activeLang: 'en',
            defaultLang: 'de'
        };
        const message: string = missingHandler.handle('notification.api-error', data);
        expect(message).toEqual('The service cannot be reached');
    });

    it('#handle() should return translated German message', () => {
        const data: TranslocoMissingHandlerData = {
            availableLangs: undefined,
            failedRetries: 0,
            flatten: {aot: false},
            interpolation: ['', ''],
            missingHandler: {allowEmpty: false, logMissingKey: false, useFallbackTranslation: false},
            prodMode: false,
            reRenderOnLangChange: false,
            activeLang: 'de',
            defaultLang: 'de'
        };
        const message: string = missingHandler.handle('notification.api-error', data);
        expect(message).toEqual('Der Service ist nicht erreichbar');
    });
});

describe('TranslocoCustomInterceptor', () => {
    let translocoCustomInterceptor: TranslocoCustomInterceptor;

    beforeEach(() => {
        translocoCustomInterceptor = TestBed.inject(TranslocoCustomInterceptor);
    });

    it('should create an instance', () => {
        expect(translocoCustomInterceptor).toBeDefined();
    });

    it('#preSaveTranslation() should not change translation object', () => {
        const translation: Translation = {
            key: 'value'
        };

        const result: Translation = translocoCustomInterceptor.preSaveTranslation(translation, 'de');

        expect(result['key']).toEqual('value');
    });

    it('#preSaveTranslationKey() should not change value', () => {
        const result: string = translocoCustomInterceptor.preSaveTranslationKey('key', 'value', 'de');

        expect(result).toEqual('value');
    });
});

describe('TranslocoCustomStrategy', () => {
    let translocoCustomStrategy: TranslocoCustomStrategy;

    beforeEach(() => {
        translocoCustomStrategy = TestBed.inject(TranslocoCustomStrategy);
    });

    it('should create an instance', () => {
        expect(translocoCustomStrategy).toBeDefined();
    });

    it('#getNextLangs() should return DE language on fail', () => {
        const result: string[] = translocoCustomStrategy.getNextLangs('en');

        expect(result).toEqual(['de']);
    });
});
