import {HttpClient} from '@angular/common/http';
import {
    DefaultTranspiler,
    getBrowserLang,
    Translation,
    TRANSLOCO_CONFIG,
    TRANSLOCO_FALLBACK_STRATEGY,
    TRANSLOCO_INTERCEPTOR,
    TRANSLOCO_LOADER,
    TRANSLOCO_MISSING_HANDLER,
    TRANSLOCO_TRANSPILER,
    translocoConfig,
    TranslocoFallbackStrategy,
    TranslocoInterceptor,
    TranslocoLoader,
    TranslocoMissingHandler,
    TranslocoMissingHandlerData,
    TranslocoModule,
    TranslocoTestingModule,
    TranslocoTestingOptions
} from '@ngneat/transloco';
import {Injectable, NgModule} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import en from '../../../assets/i18n/en.json';
import de from '../../../assets/i18n/de.json';
import i18nCriticalEn from '../../../assets/i18n/en.critical.json';
import i18nCriticalDe from '../../../assets/i18n/de.critical.json';
import {
    DefaultDateTransformer,
    DefaultNumberTransformer,
    provideTranslocoLocale,
    TRANSLOCO_DATE_TRANSFORMER,
    TRANSLOCO_LOCALE_CONFIG,
    TRANSLOCO_LOCALE_CURRENCY_MAPPING,
    TRANSLOCO_LOCALE_DEFAULT_CURRENCY,
    TRANSLOCO_LOCALE_DEFAULT_LOCALE,
    TRANSLOCO_LOCALE_LANG_MAPPING,
    TRANSLOCO_NUMBER_TRANSFORMER,
    TranslocoLocaleModule
} from '@ngneat/transloco-locale';

export const getTranslocoTestingModule = (options: TranslocoTestingOptions = {}) => TranslocoTestingModule.forRoot({
    langs: {de, en},
    translocoConfig: {
        availableLangs: ['de', 'en'],
        defaultLang: 'en',
    },
    preloadLangs: true,
    ...options
});

export const getTranslocoLocaleTestingProvider = () => provideTranslocoLocale({
    defaultLocale: 'en',
    langToLocaleMapping: {
        en: 'en-GB',
        de: 'de-DE'
    }
});

function getDefaultLanguage(): string {
    const browserLang = getBrowserLang();
    let defaultLanguage = 'de';
    if (browserLang !== 'de') {
        defaultLanguage = 'en';
    }
    return defaultLanguage;
}

interface CriticalTranslation {
    en: Record<string, string>;
    de: Record<string, string>;
}

@Injectable({
    providedIn: 'root'
})
export class TranslocoHttpLoader implements TranslocoLoader {

    constructor(private http: HttpClient) {
    }

    getTranslation(lang: string): Observable<Translation> {
        return this.http.get<Translation>(`/assets/i18n/${lang}.json`);
    }
}

@Injectable({
    providedIn: 'root'
})
export class TranslocoCustomMissingHandler implements TranslocoMissingHandler {

    private readonly criticalI18n: CriticalTranslation = {
        en: {},
        de: {}
    };

    constructor() {
        this.criticalI18n.en = this.flattenObj(i18nCriticalEn, undefined);
        this.criticalI18n.de = this.flattenObj(i18nCriticalDe, undefined);
    }

    handle(key: string, data: TranslocoMissingHandlerData): string {
        const activeLang: string = data.activeLang;
        return this.criticalI18n[activeLang][key] || key;
    }

    private flattenObj(obj, parent, res = {}): Record<string, string> {
        for (const key of Object.keys(obj)) {
            const propName = parent ? parent + '.' + key : key;
            if (Object.hasOwn(obj, key) && typeof obj[key] === 'object') {
                this.flattenObj(obj[key], propName, res);
            } else {
                res[propName] = obj[key];
            }
        }
        return res;
    }
}

@Injectable({
    providedIn: 'root'
})
export class TranslocoCustomInterceptor implements TranslocoInterceptor {

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    preSaveTranslation(translation: Translation, _lang: string): Translation {
        return translation;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    preSaveTranslationKey(_key: string, value: string, _lang: string): string {
        return value;
    }
}

@Injectable({
    providedIn: 'root'
})
export class TranslocoCustomStrategy implements TranslocoFallbackStrategy {

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    getNextLangs(_failedLang: string): string[] {
        return ['de'];
    }
}

@NgModule({
    exports: [
        TranslocoModule,
        TranslocoLocaleModule
    ],
    providers: [
        {
            provide: TRANSLOCO_CONFIG,
            useValue: translocoConfig({
                availableLangs: ['de', 'en'],
                defaultLang: getDefaultLanguage(),
                reRenderOnLangChange: true,
                prodMode: environment.production,
            })
        },
        {
            provide: TRANSLOCO_LOCALE_CONFIG,
            useValue: provideTranslocoLocale({
                localeConfig: {
                    global: {
                        date: {
                            dateStyle: 'long',
                            timeStyle: 'medium'
                        }
                    }
                },
            })
        },
        {provide: TRANSLOCO_TRANSPILER, useClass: DefaultTranspiler},
        {provide: TRANSLOCO_INTERCEPTOR, useClass: TranslocoCustomInterceptor},
        {provide: TRANSLOCO_LOADER, useClass: TranslocoHttpLoader},
        {provide: TRANSLOCO_MISSING_HANDLER, useClass: TranslocoCustomMissingHandler},
        {provide: TRANSLOCO_FALLBACK_STRATEGY, useClass: TranslocoCustomStrategy},
        {provide: TRANSLOCO_NUMBER_TRANSFORMER, useClass: DefaultNumberTransformer},
        {provide: TRANSLOCO_DATE_TRANSFORMER, useClass: DefaultDateTransformer},
        {provide: TRANSLOCO_LOCALE_DEFAULT_LOCALE, useValue: getDefaultLanguage()},
        {provide: TRANSLOCO_LOCALE_DEFAULT_CURRENCY, useValue: ''},
        {provide: TRANSLOCO_LOCALE_LANG_MAPPING, useValue: {de: 'de-DE', en: 'en-GB'}},
        {provide: TRANSLOCO_LOCALE_CURRENCY_MAPPING, useValue: {}}
    ]
})
export class TranslocoRootModule {
}
