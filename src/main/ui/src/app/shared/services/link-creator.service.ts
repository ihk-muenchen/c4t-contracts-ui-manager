import {Injectable} from '@angular/core';
import {TranslocoService} from '@ngneat/transloco';
import {Location} from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class LinkCreatorService {

    constructor(private _translocoService: TranslocoService,
                private location: Location) {
    }

    public get translocoService(): TranslocoService {
        return this._translocoService;
    }

    public prepareCheckCert4TrustLink(): string {
        const hostname: string = this.location.normalize(window.location.host);
        const isStaging: boolean = hostname.includes('-stg');
        const stagingSuffix: string = isStaging ? '-stg' : '';
        return `https://check${stagingSuffix}.cert4trust.de`;
    }

    public prepareOnboardingCert4TrustLink(): string {
        const hostname: string = this.location.normalize(window.location.host);
        const isStaging: boolean = hostname?.includes('-stg');
        const stagingSuffix: string = isStaging ? '-stg' : '';
        return `https://onboarding${stagingSuffix}.cert4trust.de`;
    }

    public prepareCert4TrustLink(): string {
        let cert4TrustLink = 'https://cert4trust.de';
        const activeLang: string = this._translocoService.getActiveLang();
        if (activeLang === 'en') {
            cert4TrustLink = cert4TrustLink.concat('/en/');
        }
        return cert4TrustLink;
    }
}
