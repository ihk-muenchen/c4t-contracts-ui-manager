import {Injectable} from '@angular/core';
import {DeviceDetectorService} from 'ngx-device-detector';

@Injectable({
    providedIn: 'root'
})
export class UnsupportedBrowserService {

    private supportedBrowsers = ['Chrome', 'Firefox'];

    constructor(private deviceDetectorService: DeviceDetectorService) {
    }

    public isUnsupportedBrowser(): boolean {
        const usedBrowser = this.deviceDetectorService.getDeviceInfo().browser;
        return this.supportedBrowsers.indexOf(usedBrowser) < 0;
    }
}
