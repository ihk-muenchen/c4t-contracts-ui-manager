import {NotificationService} from './notification.service';
import {TestBed} from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA, Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {getTranslocoLocaleTestingProvider, getTranslocoTestingModule} from '../modules/transloco-root.module';


@Injectable()
class MockMatSnackBar {

    static lastMessage = '';
    static lastAction = '';
    static lastConfig = {};

    open(message, action, config) {
        MockMatSnackBar.lastMessage = message;
        MockMatSnackBar.lastAction = action;
        MockMatSnackBar.lastConfig = config;
    }
}

describe('NotificationService', () => {
    let service;

    beforeEach(() => {
        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [
                getTranslocoTestingModule()
            ],
            providers: [
                getTranslocoLocaleTestingProvider(),
                {
                    provide: MatSnackBar,
                    useClass: MockMatSnackBar
                }
            ]
        }).compileComponents();

        service = TestBed.inject(NotificationService);
    });

    it('should run #default() ', async () => {
        service.default('abc', 'empty');
        expect(MockMatSnackBar.lastMessage).toEqual('abc');
        expect(MockMatSnackBar.lastAction).toEqual('empty');
        expect(MockMatSnackBar.lastConfig).toEqual({
            duration: 5000,
            horizontalPosition: 'center',
            verticalPosition: 'top'
        });
    });

    it('should run #success() ', async () => {
        service.success('abc', 'empty');
        expect(MockMatSnackBar.lastMessage).toEqual('abc');
        expect(MockMatSnackBar.lastAction).toEqual('empty');
        expect(MockMatSnackBar.lastConfig).toEqual({
            duration: 5000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'successSnackBar'
        });
    });

    it('should run #warning() ', async () => {
        service.warning('abc', 'empty');
        expect(MockMatSnackBar.lastMessage).toEqual('abc');
        expect(MockMatSnackBar.lastAction).toEqual('empty');
        expect(MockMatSnackBar.lastConfig).toEqual({
            duration: 5000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'warningSnackBar'
        });
    });

    it('should run #error() ', async () => {
        service.error('abc', 'empty');
        expect(MockMatSnackBar.lastMessage).toEqual('abc');
        expect(MockMatSnackBar.lastAction).toEqual('empty');
        expect(MockMatSnackBar.lastConfig).toEqual({
            duration: 3000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'errorSnackBar'
        });
    });


    it('should run #default() duration 200', async () => {
        service.default('abc', 'empty', 200);
        expect(MockMatSnackBar.lastMessage).toEqual('abc');
        expect(MockMatSnackBar.lastAction).toEqual('empty');
        expect(MockMatSnackBar.lastConfig).toEqual({
            duration: 200,
            horizontalPosition: 'center',
            verticalPosition: 'top'
        });
    });

    it('should run #success() duration 200', async () => {
        service.success('abc', 'empty', 200);
        expect(MockMatSnackBar.lastMessage).toEqual('abc');
        expect(MockMatSnackBar.lastAction).toEqual('empty');
        expect(MockMatSnackBar.lastConfig).toEqual({
            duration: 200,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'successSnackBar'
        });
    });

    it('should run #warning() duration 200', async () => {
        service.warning('abc', 'empty', 200);
        expect(MockMatSnackBar.lastMessage).toEqual('abc');
        expect(MockMatSnackBar.lastAction).toEqual('empty');
        expect(MockMatSnackBar.lastConfig).toEqual({
            duration: 200,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'warningSnackBar'
        });
    });

    it('should run #error() duration 200', async () => {
        service.error('abc', 'empty', 200);
        expect(MockMatSnackBar.lastMessage).toEqual('abc');
        expect(MockMatSnackBar.lastAction).toEqual('empty');
        expect(MockMatSnackBar.lastConfig).toEqual({
            duration: 200,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'errorSnackBar'
        });
    });

});
