import {TestBed} from '@angular/core/testing';

import {LinkCreatorService} from './link-creator.service';
import {getTranslocoTestingModule} from '../modules/transloco-root.module';
import {TranslocoService} from '@ngneat/transloco';
import {Location} from '@angular/common';

describe('LinkCreatorService', () => {
    let service: LinkCreatorService;
    let locationSpy: jasmine.SpyObj<Location>;

    beforeEach(() => {
        locationSpy = jasmine.createSpyObj('Location', ['normalize']);

        TestBed.configureTestingModule({
            imports: [
                getTranslocoTestingModule(),
            ],
            providers: [
                TranslocoService,
                {provide: Location, useValue: locationSpy}
            ]
        });
        service = TestBed.inject(LinkCreatorService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('#prepareCheckCert4TrustLink() should return link with suffix', () => {
        locationSpy.normalize.and.returnValue('onboarding-stg.cert4trust.de');

        const result: string = service.prepareCheckCert4TrustLink();
        expect(result).toEqual('https://check-stg.cert4trust.de');
    });

    it('#prepareCheckCert4TrustLink() should return link without suffix', () => {
        locationSpy.normalize.and.returnValue('onboarding.cert4trust.de');

        const result: string = service.prepareCheckCert4TrustLink();
        expect(result).toEqual('https://check.cert4trust.de');
    });

    it('#prepareOnboardingCert4TrustLink() should return link with suffix', () => {
        locationSpy.normalize.and.returnValue('check-stg.cert4trust.de');

        const result: string = service.prepareOnboardingCert4TrustLink();
        expect(result).toEqual('https://onboarding-stg.cert4trust.de');
    });

    it('#prepareOnboardingCert4TrustLink() should return link without suffix', () => {
        locationSpy.normalize.and.returnValue('check.cert4trust.de');

        const result: string = service.prepareOnboardingCert4TrustLink();
        expect(result).toEqual('https://onboarding.cert4trust.de');
    });

    it('#prepareCert4TrustLink() should return German version of link', () => {
        const translocoService = TestBed.inject(TranslocoService);
        translocoService.setActiveLang('de');

        const result: string = service.prepareCert4TrustLink();
        expect(result).toEqual('https://cert4trust.de');
    });

    it('#prepareCert4TrustLink() should return English version of link', () => {
        const translocoService = TestBed.inject(TranslocoService);
        translocoService.setActiveLang('en');

        const result: string = service.prepareCert4TrustLink();
        expect(result).toEqual('https://cert4trust.de/en/');
    });
});
