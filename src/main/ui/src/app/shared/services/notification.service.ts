import {Injectable} from '@angular/core';
import {MatSnackBar, MatSnackBarRef, TextOnlySnackBar} from '@angular/material/snack-bar';
import {TranslateService} from './translate.service';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    constructor(private snackBar: MatSnackBar,
                private translateService: TranslateService) {
    }

    default(key: string, action?: string, durationMs = 5000): void {
        const message = this.translateService.translate(key);
        this.snackBar.open(message, action, {
            duration: durationMs,
            horizontalPosition: 'center',
            verticalPosition: 'top'
        });
    }

    success(key: string, action?: string, durationMs = 5000): MatSnackBarRef<TextOnlySnackBar> {
        const message = this.translateService.translate(key);
        return this.snackBar.open(message, action, {
            duration: durationMs,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'successSnackBar'
        });
    }

    warning(key: string, action?: string, durationMs = 5000): void {
        const message = this.translateService.translate(key);
        this.snackBar.open(message, action, {
            duration: durationMs,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'warningSnackBar'
        });
    }

    error(key: string, action?: string, durationMs = 3000): void {
        const message = this.translateService.translate(key);
        this.snackBar.open(message, action, {
            duration: durationMs,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'errorSnackBar'
        });
    }
}
