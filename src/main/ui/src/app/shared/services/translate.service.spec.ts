import {TranslateService} from './translate.service';
import {TestBed, waitForAsync} from '@angular/core/testing';
import {TranslocoService} from '@ngneat/transloco';
import {getTranslocoLocaleTestingProvider, getTranslocoTestingModule} from '../modules/transloco-root.module';

describe('TranslateService', () => {

    let service: TranslateService;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                getTranslocoTestingModule()
            ],
            providers: [
                getTranslocoLocaleTestingProvider(),
                TranslocoService
            ]
        });

        service = TestBed.inject(TranslateService);
    }));

    it('should create an instance', () => {
        expect(service).toBeTruthy();
    });

    it('#checkSelectedLanguage() should get selected language from service', () => {
        sessionStorage.clear();

        service.checkSelectedLanguage();

        expect(sessionStorage.getItem('cert4trust-lang')).toEqual('en');
    });

    it('#getAvailableLanguages() should return [de, en]', () => {
        const languages = service.getAvailableLanguages();
        expect(languages).toEqual(['de', 'en']);
    });

    it('#selectLanguage() should change from "de" to "en"', () => {
        const translocoService: TranslocoService = TestBed.inject(TranslocoService);
        service.selectLanguage('en');
        const currentLanguage: string = translocoService.getActiveLang();
        expect(currentLanguage).toEqual('en');
    });

    it('#isActiveLanguage() should "en" be active', () => {
        service.selectLanguage('en');
        const result: boolean = service.isActiveLanguage('en');
        expect(result).toBeTruthy();
    });

    it('#isActiveLanguage() should "en" be inactive', () => {
        service.selectLanguage('de');
        const result: boolean = service.isActiveLanguage('en');
        expect(result).toBeFalsy();
    });

    it('#translate() should translate "notification.api-error" in "de"', () => {
        service.selectLanguage('de');
        const result: string = service.translate('notification.api-error');
        expect(result).toEqual('Der Service ist nicht erreichbar');
    });

    it('#translate() should translate "notification.api-error" in "en"', () => {
        service.selectLanguage('en');
        const result: string = service.translate('notification.api-error');
        expect(result).toEqual('The service cannot be reached');
    });

    it('#translateMessage() should translate with prefix in "de"', () => {
        service.selectLanguage('de');
        const result: string = service.translateMessage('notification', 'api error');
        expect(result).toEqual('Der Service ist nicht erreichbar');
    });

    it('#translateMessage() should translate with prefix in "en"', () => {
        service.selectLanguage('en');
        const result: string = service.translateMessage('notification', 'api error');
        expect(result).toEqual('The service cannot be reached');
    });

    it('#translateMessage() should translate with prefix in "en" (key has been not found)', () => {
        service.selectLanguage('en');
        const result: string = service.translateMessage('', 'api');
        expect(result).toEqual('.api');
    });

    it('#translateMessage() should translate with prefix in "en" (no message)', () => {
        service.selectLanguage('en');
        const result: string = service.translateMessage('key', null);
        expect(result).toEqual('key.');
    });
});
