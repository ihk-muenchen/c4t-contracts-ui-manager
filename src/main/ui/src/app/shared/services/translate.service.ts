import {Injectable} from '@angular/core';
import {HashMap, TranslocoService} from '@ngneat/transloco';
import {AvailableLangs} from '@ngneat/transloco/lib/types';
import {TranslocoLocaleService} from '@ngneat/transloco-locale';
import {Observable} from 'rxjs';

const LANGUAGE_SESSION_STORAGE_KEY = 'cert4trust-lang';

@Injectable({
    providedIn: 'root'
})
export class TranslateService {

    constructor(private translocoService: TranslocoService,
                private translocoLocaleService: TranslocoLocaleService) {
        this.checkSelectedLanguage();
    }

    checkSelectedLanguage(): void {
        const selectedLanguageStorage = sessionStorage.getItem(LANGUAGE_SESSION_STORAGE_KEY);
        const selectedLanguage = selectedLanguageStorage || this.translocoService.getActiveLang();
        this.selectLanguage(selectedLanguage);
    }

    getAvailableLanguages(): string[] | AvailableLangs {
        return this.translocoService.getAvailableLangs();
    }

    selectLanguage(lang: string): void {
        this.translocoService.setActiveLang(lang);
        const locale: string = lang === 'de' ? 'de-DE' : 'en-GB';
        this.translocoLocaleService.setLocale(locale);
        sessionStorage.setItem(LANGUAGE_SESSION_STORAGE_KEY, lang);
    }

    isActiveLanguage(lang: string): boolean {
        return lang === this.translocoService.getActiveLang();
    }

    translate(key: string, params?: HashMap): string {
        const selectedLanguage = this.translocoService.getActiveLang();
        return this.translocoService.translate(key, params, selectedLanguage);
    }

    translateMessage(prefix: string, message: string, params?: HashMap): string {
        const convertedMessage = message ? message.replace(' ', '-') : '';
        const key = `${prefix}.${convertedMessage}`;
        return this.translate(key, params);
    }

    langChanged(): Observable<string> {
        return this.translocoService.langChanges$;
    }
}
