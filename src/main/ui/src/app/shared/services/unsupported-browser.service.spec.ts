import {TestBed} from '@angular/core/testing';
import {UnsupportedBrowserService} from './unsupported-browser.service';
import {Injectable} from '@angular/core';
import {DeviceDetectorService, DeviceInfo} from 'ngx-device-detector';

@Injectable()
class MockDeviceDetectorService {

    static browser = 'Chrome';

    getDeviceInfo(): DeviceInfo {
        return {
            browser: MockDeviceDetectorService.browser,
            browser_version: '',
            device: '',
            os: '',
            os_version: '',
            userAgent: '',
            orientation: '',
            deviceType: ''
        };
    }
}

describe('UnsupportedBrowserResolver', () => {

    let resolver: UnsupportedBrowserService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: DeviceDetectorService,
                    useClass: MockDeviceDetectorService
                }
            ]
        });

        resolver = TestBed.inject(UnsupportedBrowserService);
    });

    it('should create', () => {
        expect(resolver).toBeTruthy();
    });

    it('should run #resolve() Chrome browser', () => {
        MockDeviceDetectorService.browser = 'Chrome';
        expect(resolver.isUnsupportedBrowser()).toBeFalse();
    });

    it('should run #resolve() Firefox browser', () => {
        MockDeviceDetectorService.browser = 'Firefox';
        expect(resolver.isUnsupportedBrowser()).toBeFalse();
    });

    it('should run #resolve() unsupported browser', () => {
        MockDeviceDetectorService.browser = 'Edge';
        expect(resolver.isUnsupportedBrowser()).toBeTrue();
    });
});
