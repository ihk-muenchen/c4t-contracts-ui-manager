import {AbstractControl, ValidationErrors} from '@angular/forms';
import {isAddress} from 'web3-validator';

export class Utilities {

    static addressValidator(control: AbstractControl): ValidationErrors | null {
        const address: string = control.value;
        let errors: ValidationErrors | null;
        if (!Utilities.isContractAddressValid(address)) {
            errors = {
                'address': true
            };
        }
        return errors;
    }

    static formatContractAddressStart(text: string): string {
        return text.substring(0, 21);
    }

    static formatContractAddressEnd(text: string): string {
        return text.substring(21, 42);
    }

    static formatHashStart(text: string): string {
        return text.substring(0, 33);
    }

    static formatHashEnd(text: string): string {
        return text.substring(33, 66);
    }

    static isHashValid(hash: string): boolean {
        return (hash || '').length === 66;
    }

    static isContractAddressValid(contractAddress: string): boolean {
        return isAddress(contractAddress);
    }
}
