import {InjectionToken} from '@angular/core';
import Web3 from 'web3';

export enum ChainId {
    MAINNET = '1',
    LINEA = '59144',
    GANACHE = '1337',
    LEOPOLD_DEVELOPMENT = '280509',
    LEOPOLD_STAGING = '220906',
    LEOPOLD_PRODUCTION = '210102',
    LUITPOLD_DEVELOPMENT = '2805',
    LUITPOLD_STAGING = '2806',
    LUITPOLD_PRODUCTION = '2807'
}

export const UNSUPPORTED_CHAIN_ID: Map<ChainId, string> = new Map<ChainId, string>([
    [ChainId.MAINNET, 'Mainnet'],
    [ChainId.LINEA, 'Linea']
]);

export const SUPPORTED_CHAIN_ID: Map<ChainId, string> = new Map<ChainId, string>([
    [ChainId.GANACHE, 'Ganache'],
    [ChainId.LEOPOLD_DEVELOPMENT, 'Leopold (development)'],
    [ChainId.LEOPOLD_STAGING, 'Leopold (staging)'],
    [ChainId.LEOPOLD_PRODUCTION, 'Leopold'],
    [ChainId.LUITPOLD_DEVELOPMENT, 'Luitpold (development)'],
    [ChainId.LUITPOLD_STAGING, 'Luitpold (staging)'],
    [ChainId.LUITPOLD_PRODUCTION, 'Luitpold']
]);

export const WEB3 = new InjectionToken<Web3>('web3', {
    providedIn: 'root',
    factory: () => {
        try {
            if ('ethereum_testing_web3' in window) {
                return window['ethereum_testing_web3'] as Web3;
            } else {
                const provider = ('ethereum' in window) ? window['ethereum'] : Web3.givenProvider;
                return new Web3(provider);
            }
        } catch (err) {
            throw new Error('Non-Ethereum browser detected. You should consider trying Mist or MetaMask! ' + err.toString());
        }
    }
});
