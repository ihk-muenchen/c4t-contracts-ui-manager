import {Utilities} from './utilities';

describe('Utilities', () => {

    it('should format contract address start', () => {
        const testString = '0x73b69304Ea866a8d4baE183d8728BFf35AD4d19B';
        expect(Utilities.formatContractAddressStart(testString)).toEqual('0x73b69304Ea866a8d4ba');
    });

    it('should format contract address end', () => {
        const testString = '0x73b69304Ea866a8d4baE183d8728BFf35AD4d19B';
        expect(Utilities.formatContractAddressEnd(testString)).toEqual('E183d8728BFf35AD4d19B');
    });

    it('should format contract address end with shorter string', () => {
        const testString = '0x73b69304Ea866a8d4baE183d8728BFf35AD4d19';
        expect(Utilities.formatContractAddressEnd(testString)).toEqual('E183d8728BFf35AD4d19');
    });

    it('should format contract address end with short string', () => {
        const testString = '0x73b69304Ea866';
        expect(Utilities.formatContractAddressEnd(testString)).toEqual('');
    });

    it('should format hash start', () => {
        const testString = '0xe887b2f083866a96f0cd14cc5d54f1cee090fa68ddcf307c39a192459e056cf4';
        expect(Utilities.formatHashStart(testString)).toEqual('0xe887b2f083866a96f0cd14cc5d54f1c');
    });

    it('should format hash end', () => {
        const testString = '0xe887b2f083866a96f0cd14cc5d54f1cee090fa68ddcf307c39a192459e056cf4';
        expect(Utilities.formatHashEnd(testString)).toEqual('ee090fa68ddcf307c39a192459e056cf4');
    });

    it('should format hash end with shorter string', () => {
        const testString = '0xe887b2f083866a96f0cd14cc5d54f1cee090fa68ddcf307c39a192459e056c';
        expect(Utilities.formatHashEnd(testString)).toEqual('ee090fa68ddcf307c39a192459e056c');
    });

    it('should hash be valid', () => {
        const testString = '0xe887b2f083866a96f0cd14cc5d54f1cee090fa68ddcf307c39a192459e056cf4';
        expect(Utilities.isHashValid(testString)).toBeTrue();
    });

    it('should hash be invalid (shorter length)', () => {
        const testString = '0xe887b2f083866a96f0cd14cc5d54f1cee090fa68ddcf307c39a192459e056c';
        expect(Utilities.isHashValid(testString)).toBeFalse();
    });

    it('should hash be invalid (larger length)', () => {
        const testString = '0xe887b2f083866a96f0cd14cc5d54f1cee090fa68ddcf307c39a192459e056cdasdasdasdas';
        expect(Utilities.isHashValid(testString)).toBeFalse();
    });

    it('should hash be invalid (empty string)', () => {
        const testString = '';
        expect(Utilities.isHashValid(testString)).toBeFalse();
    });

    it('should hash be invalid (null string)', () => {
        const testString = null;
        expect(Utilities.isHashValid(testString)).toBeFalse();
    });

    it('should hash be invalid (undefined string)', () => {
        const testString = undefined;
        expect(Utilities.isHashValid(testString)).toBeFalse();
    });

    it('should contract address be valid', () => {
        const testString = '0x73b69304Ea866a8d4baE183d8728BFf35AD4d19B';
        expect(Utilities.isContractAddressValid(testString)).toBeTrue();
    });

    it('should contract address be invalid (shorter length)', () => {
        const testString = '0x73b69304Ea866a8d4baE183d8728BFf35AD4d';
        expect(Utilities.isContractAddressValid(testString)).toBeFalse();
    });

    it('should contract address be invalid (larger length)', () => {
        const testString = '0x73b69304Ea866a8d4baE183d8728BFf35AD4d19B4234';
        expect(Utilities.isContractAddressValid(testString)).toBeFalse();
    });

    it('should contract address be invalid (empty string)', () => {
        const testString = '';
        expect(Utilities.isContractAddressValid(testString)).toBeFalse();
    });

    it('should contract address be invalid (null string)', () => {
        const testString = null;
        expect(Utilities.isContractAddressValid(testString)).toBeFalse();
    });

    it('should contract address be invalid (undefined string)', () => {
        const testString = undefined;
        expect(Utilities.isContractAddressValid(testString)).toBeFalse();
    });
});
