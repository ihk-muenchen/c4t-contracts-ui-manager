import {TestBed} from '@angular/core/testing';
import {WEB3} from './web3';
import Web3 from 'web3';
import PrivateKeyProvider from 'truffle-privatekey-provider';

describe('Web3', () => {

    let sut: Web3;

    it('should window.ethereum be invalid', () => {
        // @ts-expect-error Unrecognized property from Metamask
        delete global.window.ethereum_testing_web3;
        // @ts-expect-error Unrecognized property from Metamask
        window.ethereum = 'invalid';

        expect(function () {
            TestBed.inject(WEB3);
        }).toThrow(new Error('Non-Ethereum browser detected. You should consider trying Mist or MetaMask! ' +
            'ProviderError: Can\'t autodetect provider for "invalid"'));
    });


    it('should Web3.givenProvider be invalid', () => {
        // @ts-expect-error Unrecognized property from Metamask
        delete global.window.ethereum_testing_web3;
        // @ts-expect-error Unrecognized property from Metamask
        delete window.ethereum;
        // @ts-expect-error Unrecognized property from Metamask
        Web3.givenProvider = 'invalid';

        expect(function () {
            TestBed.inject(WEB3);
        }).toThrow(new Error('Non-Ethereum browser detected. You should consider trying Mist or MetaMask! ' +
            'ProviderError: Can\'t autodetect provider for "invalid"'));
    });

    it('should be defined', () => {
        // @ts-expect-error Unrecognized property from Metamask
        window.ethereum_testing_web3 = new PrivateKeyProvider(
            'f0c3bcc5f51a1ac8e3114cd42aa84d3d559c1dd0cb15e60b3d1b95d1f1e24c31',
            'http://localhost:8545'
        );
        sut = TestBed.inject(WEB3);
        expect(sut).toBeTruthy();
    });

});
