import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {InfoModalComponent} from './shared/modules/app-common/components/info-modal/info-modal.component';
import {LinkCreatorService} from './shared/services/link-creator.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

    checkCert4TrustLink: string;
    onboardingCert4TrustLink: string;
    cert4TrustLink: string;

    private changeLanguageSubscription: Subscription;

    constructor(public dialog: MatDialog,
                private linkCreatorService: LinkCreatorService) {
    }

    ngOnInit(): void {
        this.prepareLinks();

        this.changeLanguageSubscription = this.linkCreatorService.translocoService.langChanges$
            .subscribe(() => {
                this.prepareLinks();
            });
    }

    ngOnDestroy(): void {
        this.changeLanguageSubscription.unsubscribe();
    }

    openInfoDialog(event: MouseEvent, type: string): void {
        event.preventDefault();
        this.dialog.open(InfoModalComponent, {
            width: '70vw',
            data: {
                type
            },
            autoFocus: false,
        });
    }

    private prepareLinks(): void {
        this.checkCert4TrustLink = this.linkCreatorService.prepareCheckCert4TrustLink();
        this.onboardingCert4TrustLink = this.linkCreatorService.prepareOnboardingCert4TrustLink();
        this.cert4TrustLink = this.linkCreatorService.prepareCert4TrustLink();
    }
}
