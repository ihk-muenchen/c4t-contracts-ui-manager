import {ComponentFixture, TestBed} from '@angular/core/testing';
import {LanguageSelectorComponent} from './language-selector.component';
import {getTranslocoLocaleTestingProvider, getTranslocoTestingModule} from '../../shared/modules/transloco-root.module';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('LanguageSelectorComponent', () => {
    let component: LanguageSelectorComponent;
    let fixture: ComponentFixture<LanguageSelectorComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA
            ],
            imports: [
                getTranslocoTestingModule()
            ],
            declarations: [
                LanguageSelectorComponent
            ],
            providers: [
                getTranslocoLocaleTestingProvider()
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LanguageSelectorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should change language', () => {
        const translateServiceSpy = spyOn(component['translateService'], 'selectLanguage');
        component.selectLanguage('de');
        expect(translateServiceSpy).toHaveBeenCalledWith('de');
    });
});
