import {Component, OnInit} from '@angular/core';
import {TranslateService} from '../../shared/services/translate.service';
import {AvailableLangs} from '@ngneat/transloco/lib/types';

@Component({
    selector: 'app-language-selector',
    templateUrl: './language-selector.component.html',
    styleUrls: ['./language-selector.component.scss']
})
export class LanguageSelectorComponent implements OnInit {

    availableLanguages: string[] | AvailableLangs;

    constructor(private translateService: TranslateService) {
    }

    ngOnInit(): void {
        this.availableLanguages = this.translateService.getAvailableLanguages();
    }

    selectLanguage(lang: string): void {
        this.translateService.selectLanguage(lang);
    }

    isActiveLanguage(lang: string): boolean {
        return this.translateService.isActiveLanguage(lang);
    }
}
