import {ComponentFixture, TestBed} from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA, Injectable} from '@angular/core';
import {AppComponent} from './app.component';
import {MatDialog} from '@angular/material/dialog';
import {getTranslocoLocaleTestingProvider, getTranslocoTestingModule} from './shared/modules/transloco-root.module';
import {TranslocoTestingOptions} from '@ngneat/transloco';
import {Router} from '@angular/router';

const TRANSLOCO_TESTING_OPTIONS: TranslocoTestingOptions = {
    translocoConfig: {
        defaultLang: 'de'
    },
    preloadLangs: true
};

@Injectable()
class MockRouter {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    navigate() {
    }
}

@Injectable()
class MockMatDialog {

    wasOpened = false;
    wasOpenedByType = null;

    open(clazz) {
        this.wasOpened = true;
        this.wasOpenedByType = clazz.name;
    }

}

describe('AppComponent', () => {

    let fixture: ComponentFixture<AppComponent>;
    let component: AppComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                getTranslocoTestingModule(TRANSLOCO_TESTING_OPTIONS)
            ],
            declarations: [
                AppComponent
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                getTranslocoLocaleTestingProvider(),
                {
                    provide: MatDialog,
                    useClass: MockMatDialog
                },
                {
                    provide: Router,
                    useClass: MockRouter
                }
            ]
        }).overrideComponent(AppComponent, {}).compileComponents();
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.debugElement.componentInstance;
        fixture.detectChanges();
    });

    afterEach(() => {
        fixture.destroy();
    });

    it('should run #constructor()', async () => {
        expect(component).toBeTruthy();
    });

    it('should run #openInfoDialog()', async () => {
        const dialogSpy = spyOn(component['dialog'], 'open');
        component.openInfoDialog(new MouseEvent('click'), 'impressum');
        expect(dialogSpy).toHaveBeenCalled();
    });
});
