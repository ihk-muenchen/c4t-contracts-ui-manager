// @ts-check
const eslint = require("@eslint/js");
const tseslint = require("typescript-eslint");
const angular = require("angular-eslint");

const eslintPluginImport = require("eslint-plugin-import");
const eslintPluginJsdoc = require("eslint-plugin-jsdoc");

const typescriptConfigRules = {
    "@angular-eslint/directive-selector": [
        "error",
        {
            type: "attribute",
            prefix: "app",
            style: "camelCase",
        },
    ],
    "@angular-eslint/component-selector": [
        "error",
        {
            type: "element",
            prefix: "app",
            style: "kebab-case",
        },
    ],
    "@typescript-eslint/consistent-type-definitions": "error",
    "@typescript-eslint/dot-notation": "off",
    "@typescript-eslint/explicit-member-accessibility": [
        "off",
        {
            "accessibility": "explicit"
        }
    ],
    "@typescript-eslint/member-ordering": "error",
    "@typescript-eslint/naming-convention": "off",
    "@typescript-eslint/no-empty-interface": "error",
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/no-inferrable-types": [
        "error",
        {
            "ignoreParameters": true
        }
    ],
    "@typescript-eslint/no-misused-new": "error",
    "@typescript-eslint/no-non-null-assertion": "error",
    "@typescript-eslint/no-shadow": [
        "error",
        {
            "hoist": "all"
        }
    ],
    "@typescript-eslint/no-unused-expressions": "error",
    "@typescript-eslint/no-use-before-define": "error",
    "@typescript-eslint/prefer-function-type": "error",
    "@/quotes": [
        "error",
        "single"
    ],
    "object-curly-spacing": [
        "error",
        "never"
    ],
    "@typescript-eslint/unified-signatures": "error",
    "arrow-body-style": "error",
    "constructor-super": "error",
    "curly": "error",
    "dot-notation": "off",
    "eqeqeq": [
        "error",
        "smart"
    ],
    "guard-for-in": "error",
    "id-denylist": "off",
    "id-match": "off",
    "max-len": [
        "error",
        {
            "code": 140
        }
    ],
    "no-bitwise": "error",
    "no-caller": "error",
    "no-console": [
        "error",
        {
            "allow": [
                "log",
                "warn",
                "dir",
                "timeLog",
                "assert",
                "clear",
                "count",
                "countReset",
                "group",
                "groupEnd",
                "table",
                "dirxml",
                "error",
                "groupCollapsed",
                "Console",
                "profile",
                "profileEnd",
                "timeStamp",
                "context"
            ]
        }
    ],
    "no-debugger": "error",
    "no-empty": "off",
    "no-empty-function": "off",
    "no-eval": "error",
    "no-fallthrough": "error",
    "no-new-wrappers": "error",
    "no-restricted-imports": [
        "error",
        "rxjs/Rx"
    ],
    "no-shadow": "error",
    "no-throw-literal": "error",
    "no-undef-init": "error",
    "no-underscore-dangle": "off",
    "no-unused-expressions": "error",
    "no-unused-labels": "error",
    "no-use-before-define": "error",
    "no-var": "error",
    "prefer-const": "error",
    "radix": "error",
    "spaced-comment": [
        "error",
        "always",
        {
            "markers": [
                "/"
            ]
        }
    ]
};

module.exports = tseslint.config(
    {
        files: ["**/*.ts"],
        ignores: ["**/*.spec.ts"],
        extends: [
            eslint.configs.recommended,
            ...tseslint.configs.recommended,
            ...tseslint.configs.stylistic,
            ...angular.configs.tsRecommended,
        ],
        processor: angular.processInlineTemplates,
        plugins: {
            eslintPluginImport: eslintPluginImport,
            eslintPluginJsdoc: eslintPluginJsdoc,
            tseslint: tseslint
        },
        rules: {
            ...typescriptConfigRules
        }
    },
    {
        files: ["**/*.spec.ts"],
        extends: [
            eslint.configs.recommended,
            ...tseslint.configs.recommended,
            ...tseslint.configs.stylistic,
            ...angular.configs.tsRecommended,
        ],
        processor: angular.processInlineTemplates,
        plugins: {
            eslintPluginImport: eslintPluginImport,
            eslintPluginJsdoc: eslintPluginJsdoc,
            tseslint: tseslint
        },
        rules: {
            ...typescriptConfigRules,
            "@typescript-eslint/no-empty-function": "off",
            "@typescript-eslint/no-unused-vars": "off"
        }
    },
    {
        files: ["**/*.html"],
        extends: [
            ...angular.configs.templateRecommended,
            ...angular.configs.templateAccessibility,
        ],
        rules: {},
    }
);
